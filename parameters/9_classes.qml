<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+8" hasScaleBasedVisibilityFlag="0" version="3.6.3-Noosa" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <rasterrenderer classificationMax="255" classificationMin="0" alphaBand="-1" opacity="1" band="1" type="singlebandpseudocolor">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="EXACT" clip="0" classificationMode="2">
          <colorramp name="[source]" type="random">
            <prop k="count" v="100"/>
            <prop k="hueMax" v="359"/>
            <prop k="hueMin" v="0"/>
            <prop k="rampType" v="random"/>
            <prop k="satMax" v="240"/>
            <prop k="satMin" v="100"/>
            <prop k="valMax" v="240"/>
            <prop k="valMin" v="200"/>
          </colorramp>
          <item value="0" label="0 - Unclassified" color="#000000" alpha="255"/>
          <item value="1" label="1 - SN->SN" color="#adadad" alpha="255"/>
          <item value="2" label="2 - LowVegIncr" color="#3dd84a" alpha="255"/>
          <item value="3" label="3 - HighVegIncr" color="#289031" alpha="255"/>
          <item value="4" label="4 - SN->Water" color="#66b1cc" alpha="255"/>
          <item value="5" label="5 - Veg->SN" color="#bc8f1e" alpha="255"/>
          <item value="6" label="6 - Veg->Water" color="#2c73a9" alpha="255"/>
          <item value="7" label="7 - VegDecr" color="#e96c89" alpha="255"/>
          <item value="8" label="8 - Water->SN" color="#f1d029" alpha="255"/>
          <item value="9" label="9 - Water->Water" color="#494949" alpha="255"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation grayscaleMode="0" colorizeBlue="128" colorizeStrength="100" colorizeRed="255" saturation="0" colorizeOn="0" colorizeGreen="128"/>
    <rasterresampler zoomedOutResampler="bilinear" maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
