<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+8" hasScaleBasedVisibilityFlag="0" version="3.6.3-Noosa" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <rasterrenderer classificationMax="255" classificationMin="0" alphaBand="-1" opacity="1" band="1" type="singlebandpseudocolor">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="EXACT" clip="0" classificationMode="2">
          <colorramp name="[source]" type="random">
            <prop k="count" v="100"/>
            <prop k="hueMax" v="359"/>
            <prop k="hueMin" v="0"/>
            <prop k="rampType" v="random"/>
            <prop k="satMax" v="240"/>
            <prop k="satMin" v="100"/>
            <prop k="valMax" v="240"/>
            <prop k="valMin" v="200"/>
          </colorramp>
          <item value="0" label="0" color="#000000" alpha="255"/>
          <item value="1" label="1" color="#000000" alpha="255"/>
          <item value="2" label="2" color="#000000" alpha="255"/>
          <item value="3" label="3" color="#000000" alpha="255"/>
          <item value="4" label="4" color="#000000" alpha="255"/>
          <item value="5" label="5" color="#000000" alpha="255"/>
          <item value="6" label="6" color="#000000" alpha="255"/>
          <item value="7" label="7" color="#000000" alpha="255"/>
          <item value="8" label="8" color="#000000" alpha="255"/>
          <item value="9" label="9" color="#000000" alpha="255"/>
          <item value="10" label="10" color="#000000" alpha="255"/>
          <item value="11" label="11" color="#000000" alpha="255"/>
          <item value="12" label="12" color="#000000" alpha="255"/>
          <item value="13" label="13" color="#000000" alpha="255"/>
          <item value="14" label="14" color="#000000" alpha="255"/>
          <item value="15" label="15" color="#000000" alpha="255"/>
          <item value="16" label="16" color="#000000" alpha="255"/>
          <item value="17" label="17" color="#000000" alpha="255"/>
          <item value="18" label="18" color="#000000" alpha="255"/>
          <item value="19" label="19" color="#000000" alpha="255"/>
          <item value="20" label="20" color="#000000" alpha="255"/>
          <item value="21" label="21" color="#000000" alpha="255"/>
          <item value="22" label="22" color="#000000" alpha="255"/>
          <item value="23" label="23" color="#000000" alpha="255"/>
          <item value="24" label="24" color="#000000" alpha="255"/>
          <item value="25" label="25" color="#000000" alpha="255"/>
          <item value="26" label="26" color="#000000" alpha="255"/>
          <item value="27" label="27" color="#000000" alpha="255"/>
          <item value="28" label="28" color="#000000" alpha="255"/>
          <item value="29" label="29" color="#000000" alpha="255"/>
          <item value="30" label="30" color="#000000" alpha="255"/>
          <item value="31" label="31" color="#000000" alpha="255"/>
          <item value="32" label="32" color="#000000" alpha="255"/>
          <item value="33" label="33" color="#000000" alpha="255"/>
          <item value="34" label="34" color="#000000" alpha="255"/>
          <item value="35" label="35" color="#000000" alpha="255"/>
          <item value="36" label="36" color="#000000" alpha="255"/>
          <item value="37" label="37" color="#000000" alpha="255"/>
          <item value="38" label="38" color="#000000" alpha="255"/>
          <item value="39" label="39" color="#000000" alpha="255"/>
          <item value="40" label="40" color="#000000" alpha="255"/>
          <item value="41" label="41" color="#000000" alpha="255"/>
          <item value="42" label="42" color="#000000" alpha="255"/>
          <item value="43" label="43" color="#000000" alpha="255"/>
          <item value="44" label="44" color="#000000" alpha="255"/>
          <item value="45" label="45" color="#000000" alpha="255"/>
          <item value="46" label="46" color="#000000" alpha="255"/>
          <item value="47" label="47" color="#000000" alpha="255"/>
          <item value="48" label="48" color="#000000" alpha="255"/>
          <item value="49" label="49" color="#000000" alpha="255"/>
          <item value="50" label="50" color="#000000" alpha="255"/>
          <item value="51" label="51" color="#000000" alpha="255"/>
          <item value="52" label="52" color="#000000" alpha="255"/>
          <item value="53" label="53" color="#000000" alpha="255"/>
          <item value="54" label="54" color="#000000" alpha="255"/>
          <item value="55" label="55" color="#000000" alpha="255"/>
          <item value="56" label="56" color="#000000" alpha="255"/>
          <item value="57" label="57" color="#000000" alpha="255"/>
          <item value="58" label="58" color="#000000" alpha="255"/>
          <item value="59" label="59" color="#000000" alpha="255"/>
          <item value="60" label="60" color="#000000" alpha="255"/>
          <item value="61" label="61" color="#000000" alpha="255"/>
          <item value="62" label="62" color="#000000" alpha="255"/>
          <item value="63" label="63" color="#000000" alpha="255"/>
          <item value="64" label="64" color="#000000" alpha="255"/>
          <item value="65" label="65" color="#000000" alpha="255"/>
          <item value="66" label="66" color="#000000" alpha="255"/>
          <item value="67" label="67" color="#000000" alpha="255"/>
          <item value="68" label="68" color="#000000" alpha="255"/>
          <item value="69" label="69" color="#000000" alpha="255"/>
          <item value="70" label="70" color="#000000" alpha="255"/>
          <item value="71" label="71" color="#000000" alpha="255"/>
          <item value="72" label="72" color="#000000" alpha="255"/>
          <item value="73" label="73" color="#000000" alpha="255"/>
          <item value="74" label="74" color="#000000" alpha="255"/>
          <item value="75" label="75" color="#000000" alpha="255"/>
          <item value="76" label="76" color="#000000" alpha="255"/>
          <item value="77" label="77" color="#000000" alpha="255"/>
          <item value="78" label="78" color="#000000" alpha="255"/>
          <item value="79" label="79" color="#000000" alpha="255"/>
          <item value="80" label="80" color="#000000" alpha="255"/>
          <item value="81" label="81" color="#000000" alpha="255"/>
          <item value="82" label="82" color="#000000" alpha="255"/>
          <item value="83" label="83" color="#000000" alpha="255"/>
          <item value="84" label="84" color="#000000" alpha="255"/>
          <item value="85" label="85" color="#000000" alpha="255"/>
          <item value="86" label="86" color="#000000" alpha="255"/>
          <item value="87" label="87" color="#000000" alpha="255"/>
          <item value="88" label="88" color="#000000" alpha="255"/>
          <item value="89" label="89" color="#000000" alpha="255"/>
          <item value="90" label="90" color="#000000" alpha="255"/>
          <item value="91" label="91" color="#000000" alpha="255"/>
          <item value="92" label="92" color="#000000" alpha="255"/>
          <item value="93" label="93" color="#000000" alpha="255"/>
          <item value="94" label="94" color="#000000" alpha="255"/>
          <item value="95" label="95" color="#000000" alpha="255"/>
          <item value="96" label="96" color="#000000" alpha="255"/>
          <item value="97" label="97" color="#000000" alpha="255"/>
          <item value="98" label="98" color="#000000" alpha="255"/>
          <item value="99" label="99" color="#000000" alpha="255"/>
          <item value="100" label="100" color="#000000" alpha="255"/>
          <item value="101" label="101" color="#000000" alpha="255"/>
          <item value="102" label="102" color="#000000" alpha="255"/>
          <item value="103" label="103" color="#000000" alpha="255"/>
          <item value="104" label="104" color="#000000" alpha="255"/>
          <item value="105" label="105" color="#000000" alpha="255"/>
          <item value="106" label="106" color="#000000" alpha="255"/>
          <item value="107" label="107" color="#000000" alpha="255"/>
          <item value="108" label="108" color="#000000" alpha="255"/>
          <item value="109" label="109" color="#000000" alpha="255"/>
          <item value="110" label="110" color="#000000" alpha="255"/>
          <item value="111" label="111" color="#000000" alpha="255"/>
          <item value="112" label="112" color="#000000" alpha="255"/>
          <item value="113" label="113" color="#000000" alpha="255"/>
          <item value="114" label="114" color="#000000" alpha="255"/>
          <item value="115" label="115" color="#000000" alpha="255"/>
          <item value="116" label="116" color="#000000" alpha="255"/>
          <item value="117" label="117" color="#000000" alpha="255"/>
          <item value="118" label="118" color="#000000" alpha="255"/>
          <item value="119" label="119" color="#000000" alpha="255"/>
          <item value="120" label="120" color="#000000" alpha="255"/>
          <item value="121" label="121" color="#000000" alpha="255"/>
          <item value="122" label="122" color="#000000" alpha="255"/>
          <item value="123" label="123" color="#000000" alpha="255"/>
          <item value="124" label="124" color="#000000" alpha="255"/>
          <item value="125" label="125" color="#000000" alpha="255"/>
          <item value="126" label="126" color="#000000" alpha="255"/>
          <item value="127" label="127" color="#000000" alpha="255"/>
          <item value="128" label="128" color="#000000" alpha="255"/>
          <item value="129" label="129" color="#000000" alpha="255"/>
          <item value="130" label="130" color="#000000" alpha="255"/>
          <item value="131" label="131" color="#000000" alpha="255"/>
          <item value="132" label="132" color="#000000" alpha="255"/>
          <item value="133" label="133" color="#000000" alpha="255"/>
          <item value="134" label="134" color="#000000" alpha="255"/>
          <item value="135" label="135" color="#000000" alpha="255"/>
          <item value="136" label="136" color="#000000" alpha="255"/>
          <item value="137" label="137" color="#000000" alpha="255"/>
          <item value="138" label="138" color="#000000" alpha="255"/>
          <item value="139" label="139" color="#000000" alpha="255"/>
          <item value="140" label="140" color="#000000" alpha="255"/>
          <item value="141" label="141" color="#000000" alpha="255"/>
          <item value="142" label="142" color="#000000" alpha="255"/>
          <item value="143" label="143" color="#000000" alpha="255"/>
          <item value="144" label="144" color="#000000" alpha="255"/>
          <item value="145" label="145" color="#000000" alpha="255"/>
          <item value="146" label="146" color="#000000" alpha="255"/>
          <item value="147" label="147" color="#000000" alpha="255"/>
          <item value="148" label="148" color="#000000" alpha="255"/>
          <item value="149" label="149" color="#000000" alpha="255"/>
          <item value="150" label="150" color="#000000" alpha="255"/>
          <item value="151" label="151" color="#000000" alpha="255"/>
          <item value="152" label="152" color="#000000" alpha="255"/>
          <item value="153" label="153" color="#000000" alpha="255"/>
          <item value="154" label="154" color="#000000" alpha="255"/>
          <item value="155" label="155" color="#000000" alpha="255"/>
          <item value="156" label="156" color="#000000" alpha="255"/>
          <item value="157" label="157" color="#000000" alpha="255"/>
          <item value="158" label="158" color="#000000" alpha="255"/>
          <item value="159" label="159" color="#000000" alpha="255"/>
          <item value="160" label="160" color="#000000" alpha="255"/>
          <item value="161" label="161" color="#000000" alpha="255"/>
          <item value="162" label="162" color="#000000" alpha="255"/>
          <item value="163" label="163" color="#000000" alpha="255"/>
          <item value="164" label="164" color="#000000" alpha="255"/>
          <item value="165" label="165" color="#000000" alpha="255"/>
          <item value="166" label="166" color="#000000" alpha="255"/>
          <item value="167" label="167" color="#000000" alpha="255"/>
          <item value="168" label="168" color="#000000" alpha="255"/>
          <item value="169" label="169" color="#000000" alpha="255"/>
          <item value="170" label="170" color="#000000" alpha="255"/>
          <item value="171" label="171" color="#000000" alpha="255"/>
          <item value="172" label="172" color="#000000" alpha="255"/>
          <item value="173" label="173" color="#000000" alpha="255"/>
          <item value="174" label="174" color="#000000" alpha="255"/>
          <item value="175" label="175" color="#000000" alpha="255"/>
          <item value="176" label="176" color="#000000" alpha="255"/>
          <item value="177" label="177" color="#000000" alpha="255"/>
          <item value="178" label="178" color="#000000" alpha="255"/>
          <item value="179" label="179" color="#000000" alpha="255"/>
          <item value="180" label="180" color="#000000" alpha="255"/>
          <item value="181" label="181" color="#000000" alpha="255"/>
          <item value="182" label="182" color="#000000" alpha="255"/>
          <item value="183" label="183" color="#000000" alpha="255"/>
          <item value="184" label="184" color="#000000" alpha="255"/>
          <item value="185" label="185" color="#000000" alpha="255"/>
          <item value="186" label="186" color="#000000" alpha="255"/>
          <item value="187" label="187" color="#000000" alpha="255"/>
          <item value="188" label="188" color="#000000" alpha="255"/>
          <item value="189" label="189" color="#000000" alpha="255"/>
          <item value="190" label="190" color="#000000" alpha="255"/>
          <item value="191" label="191" color="#000000" alpha="255"/>
          <item value="192" label="192" color="#000000" alpha="255"/>
          <item value="193" label="193" color="#000000" alpha="255"/>
          <item value="194" label="194" color="#000000" alpha="255"/>
          <item value="195" label="195" color="#000000" alpha="255"/>
          <item value="196" label="196" color="#000000" alpha="255"/>
          <item value="197" label="197" color="#000000" alpha="255"/>
          <item value="198" label="198" color="#000000" alpha="255"/>
          <item value="199" label="199" color="#000000" alpha="255"/>
          <item value="200" label="200" color="#000000" alpha="255"/>
          <item value="201" label="201" color="#000000" alpha="255"/>
          <item value="202" label="202" color="#000000" alpha="255"/>
          <item value="203" label="203" color="#000000" alpha="255"/>
          <item value="204" label="204" color="#000000" alpha="255"/>
          <item value="205" label="205" color="#000000" alpha="255"/>
          <item value="206" label="206" color="#000000" alpha="255"/>
          <item value="207" label="207" color="#000000" alpha="255"/>
          <item value="208" label="208" color="#000000" alpha="255"/>
          <item value="209" label="209" color="#000000" alpha="255"/>
          <item value="210" label="210" color="#000000" alpha="255"/>
          <item value="211" label="211" color="#000000" alpha="255"/>
          <item value="212" label="212" color="#000000" alpha="255"/>
          <item value="213" label="213" color="#000000" alpha="255"/>
          <item value="214" label="214" color="#000000" alpha="255"/>
          <item value="215" label="215" color="#000000" alpha="255"/>
          <item value="216" label="216" color="#000000" alpha="255"/>
          <item value="217" label="217" color="#000000" alpha="255"/>
          <item value="218" label="218" color="#000000" alpha="255"/>
          <item value="219" label="219" color="#000000" alpha="255"/>
          <item value="220" label="220" color="#000000" alpha="255"/>
          <item value="221" label="221" color="#000000" alpha="255"/>
          <item value="222" label="222" color="#000000" alpha="255"/>
          <item value="223" label="223" color="#000000" alpha="255"/>
          <item value="224" label="224" color="#000000" alpha="255"/>
          <item value="225" label="225" color="#000000" alpha="255"/>
          <item value="226" label="226" color="#000000" alpha="255"/>
          <item value="227" label="227" color="#000000" alpha="255"/>
          <item value="228" label="228" color="#000000" alpha="255"/>
          <item value="229" label="229" color="#000000" alpha="255"/>
          <item value="230" label="230" color="#000000" alpha="255"/>
          <item value="231" label="231" color="#000000" alpha="255"/>
          <item value="232" label="232" color="#000000" alpha="255"/>
          <item value="233" label="233" color="#000000" alpha="255"/>
          <item value="234" label="234" color="#000000" alpha="255"/>
          <item value="235" label="235" color="#000000" alpha="255"/>
          <item value="236" label="236" color="#000000" alpha="255"/>
          <item value="237" label="237" color="#000000" alpha="255"/>
          <item value="238" label="238" color="#000000" alpha="255"/>
          <item value="239" label="239" color="#000000" alpha="255"/>
          <item value="240" label="240" color="#000000" alpha="255"/>
          <item value="241" label="241" color="#000000" alpha="255"/>
          <item value="242" label="242" color="#000000" alpha="255"/>
          <item value="243" label="243" color="#000000" alpha="255"/>
          <item value="244" label="244" color="#000000" alpha="255"/>
          <item value="245" label="245" color="#000000" alpha="255"/>
          <item value="246" label="246" color="#000000" alpha="255"/>
          <item value="247" label="247" color="#000000" alpha="255"/>
          <item value="248" label="248" color="#000000" alpha="255"/>
          <item value="249" label="249" color="#000000" alpha="255"/>
          <item value="250" label="250" color="#000000" alpha="255"/>
          <item value="251" label="251" color="#000000" alpha="255"/>
          <item value="252" label="252" color="#000000" alpha="255"/>
          <item value="253" label="253" color="#000000" alpha="255"/>
          <item value="254" label="254" color="#000000" alpha="255"/>
          <item value="255" label="255" color="#000000" alpha="255"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation grayscaleMode="0" colorizeBlue="128" colorizeStrength="100" colorizeRed="255" saturation="0" colorizeOn="0" colorizeGreen="128"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
