#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Pour produire si les dossiers existent déjà
@author: impact
"""
import logging

#~ from sen2chain import Tile
from sen2change import CVAProduct, CVALibrary

logger = logging.getLogger("Process_CVA_libray")
logging.basicConfig(level=logging.INFO)

lib = CVALibrary()

for cva_id in lib._38KND:
    logger.info(cva_id)
    product = CVAProduct(cva_id)
    if not product.cva:
        product.initialize()
        #~ logger.info("product.computeCVA")
        product.computeCVA()
    if not product.classif:
        #~ logger.info("product.computeClassif")
        product.computeClassif()
    if not product.classif_SM:
        product.computeSeuillageMagnitude()
    if not product.classif_SM_CM:
        #~ logger.info("product.computeClassif")
        product.computeCloudMaskClassif()
    if not product.reclassif:
        product.computeReclassif()
        
        
