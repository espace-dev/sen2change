#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exemples de commandes 
@author: impact
"""


import sen2change


#### Calcul de la librairie d'apprentissage #### 
# sans les classes "brulé" qui marche bien
c = sen2change.BuildCVARef('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_WGS84.shp', ['NDVI', 'BIGR', 'NDWIGAO'])
# avec les classes "brulé"
c = sen2change.BuildCVARef('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_03_WGS84.shp', ['NDVI', 'BIGR', 'NDWIGAO'])

c.values
c.grouped
c.means
c.std
c.changelib
c.changelib1std
c.changelib2std
c.changelib3std

c.changelib1std.to_csv('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_02_01_changelib1std.csv')
c.changelib2std.to_csv('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_02_02_changelib2std.csv')
c.changelib3std.to_csv('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_02_03_changelib3std.csv')

#### Calcul de l'image du CVA #### 
sen2change.rasterCVA3D(tile='38KND', indices=['ndvi', 'bigr', 'ndwigao'], date_bef='2017-12-25', date_aft='2018-01-')


#### Pour classer un CVA #### 
# avec la librairie par defaut
sen2change.classif_CVA('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29.jp2')
sen2change.classif_CVA('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29.jp2')
sen2change.classif_CVA('/DATA_S2/S2_CVA_SEN2CHANGE/39LVD/2017-02-10_2017-03-12/39LVD_CVA3D_NDVI-BIGR-NDWIGAO_2017-02-10_2017-03-12.jp2')
# avec une librairie par custom
sen2change.classif_CVA('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29.jp2', \
                       "/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_03_01_CVA.csv")
# MULTIPROCESSING avec la librairie par defaut
sen2change.classif_CVA_multiproc('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29.jp2')                       
                       


#### Pour créer un qml (legende QGIS) à partir d'une classif de CVA #### 
# avec la librairie par defaut
sen2change.legend_classif('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29_classification_m.jp2')
# avec une librairie par custom
sen2change.legend_classif(class_image_path = '/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29_classification_clurn.jp2', \
                          cva_changelib_path = "/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_03_01_CVA.csv")

#### Pour reclasser une classification #### 
sen2change.reclass_classif('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2017-12-25_2018-01-29/38KND_CVA3D_NDVI-BIGR-NDWIGAO_2017-12-25_2018-01-29_classification.jp2')
sen2change.reclass_classif('/DATA_S2/S2_CVA_SEN2CHANGE/39LVD/2017-02-10_2017-03-12/39LVD_CVA3D_NDVI-BIGR-NDWIGAO_2017-02-10_2017-03-12_classification.jp2')

#### Pour extraire la class WET (zones nouvellement en eau) #### 
sen2change.post_classif('/DATA_S2/S2_CVA_SEN2CHANGE/38KND/2018-04-04_2018-04-09/test_CVA3D_38KND_NDVI-BIGR-NDWIGAO_2018-04-04_2018-04-09_classification.jp2', \
                        'test_CVA3D_38KND_NDVI-BIGR-NDWIGAO_2018-04-04_2018-04-09.jp2')
                        
                        
##### #### #### #### #### #### #### #### #### #### #### #### ### 
#### Pour gerer les CVA #### 
##### ### #### #### #### #### #### #### #### #### #### #### ####

from sen2change import CVALibrary
# Librairie
lib = CVALibrary()
lib.cva
lib.computeCloudCover("38KND")


from sen2change import CVAProduct
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20180409T071211_N0206_R020_T38KND_20180409T103207"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20180109T071211_N0206_R020_T38KND_20180109T105957"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20180119T071211_N0206_R020_T38KND_20180119T105643"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20180129T071211_N0206_R020_T38KND_20180129T104706"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20171001T071211_N0205_R020_T38KND_20171001T071212"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20171001T071211_N0205_R020_T38KND_20171001T071505"
#~ identifier = "S2A_MSIL2A_20151211T071132_N0204_R020_T38KND_20151211T071132-S2A_MSIL2A_20160120T071132_N0201_R020_T38KND_20160120T071135"
#~ identifier = "S2A_MSIL2A_20171011T071211_N0205_R020_T38KND_20171011T071214-S2A_MSIL2A_20180109T071211_N0206_R020_T38KND_20180109T105957"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20151022T071142_N0204_R020_T38KND_20151022T071137"
#~ identifier = "S2B_MSIL2A_20171225T071209_N0206_R020_T38KND_20171225T102551-S2A_MSIL2A_20151121T071132_N0204_R020_T38KND_20151121T071231"
#~ CVA3D_S2A_MSIL2A_20171011T071211_N0205_R020_T38KND_20171011T071214-S2A_MSIL2A_20180109T071211_N0206_R020_T38KND_20180109T105957
#~ identifier = "S2A_MSIL2A_20171018T070231_N0205_R120_T38KQE_20171018T070233-S2B_MSIL2A_20180908T070219_N0206_R120_T38KQE_20180908T102458"
#~ identifier = "S2A_MSIL2A_20170928T070231_N0205_R120_T38KQE_20170928T070230-S2A_MSIL2A_20150929T070246_N0204_R120_T38KQE_20150929T070249"
#~ identifier = "S2A_MSIL2A_20170928T070231_N0205_R120_T38KQE_20170928T070230-S2A_MSIL2A_20180116T070231_N0206_R120_T38KQE_20180116T102048"
#~ identifier = "S2B_MSIL2A_20190816T155529_N0208_R011_T17RQK_20190816T191700-S2A_MSIL2A_20190801T155531_N0208_R011_T17RQK_20190801T204823"
#~ identifier = "S2A_MSIL2A_20190921T071211_N0208_R020_T38LPH_20190921T093700-S2B_MSIL2A_20200203T071159_N0208_R020_T38LPH_20200203T102620"
identifier = "S2A_MSIL2A_20170210T070231_N0204_R120_T39LUE_20170210T070228-S2A_MSIL2A_20170312T070231_N0204_R120_T39LUE_20170312T070226"
#~ identifier = "S2A_MSIL2A_20170210T070231_N0204_R120_T39LVE_20170210T070228-S2A_MSIL2A_20170312T070231_N0204_R120_T39LVE_20170312T070226"
#~ identifier = "S2A_MSIL2A_20170210T070231_N0204_R120_T39LVD_20170210T070228-S2A_MSIL2A_20170312T070231_N0204_R120_T39LVD_20170312T070226"
identifier = "S2A_MSIL2A_20170210T070231_N0204_R120_T39LUD_20170210T070228-S2A_MSIL2A_20170312T070231_N0204_R120_T39LUD_20170312T070226"



### Charger le CVA product
toto = CVAProduct(identifier)
# Avec la date du cyclone
toto = CVAProduct(identifier, "20180106")

toto.exists
toto.path
toto.cva
toto.magnitude
toto.classif
toto.distance
toto.classif_SM
toto.classif_SM_CM
toto.reclassif
toto.temporalcontext


# Tout lancer d'un coup
toto.computeAll()

# Initialiser 
toto.initialize()

# Calculer le CVA
toto.computeCVA()

# Classer l'image
toto.computeClassif()

# Masquer les faibles magnitudes
toto.computeSeuillageMagnitude()
toto.computeSeuillageMagnitude(mode=1, seuil=4000)
toto.computeSeuillageMagnitude(mode=2)

# Masquer la classif avec les nuages
toto.computeCloudMaskClassif()

# Reclasser la classif
toto.computeReclassif()

# Calcul contexte temporel (sameclass cva)
toto.computeTemporalContext()

# Calcul contexte temporel (No Change)
toto.computeRefNoChange()

# Calcul contexte temporel (Class Diversity)
toto.computeRefClassDiversity()

# Calcul contexte temporel (zones en eau ref)
toto.computeRefWaterExtent()
# Calcul contexte temporel multiprocessing (zones en eau ref)
toto.computeRefWaterExtent_multiproc()

