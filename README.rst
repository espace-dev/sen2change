Sen2change
=========

Sen2change is a change detection toolbox for Sentinel-2 products.

Main features:

Sen2change was developped in the frame of RenovRisk Impact project (ESPACE-DEV/IRD/Université de La Réunion) (2018-2021),

Principle
============

Change detection is based on CVA product calculation which stand for Change Vector Analysis. 
The technique was developed to study the impact of meteorological events such as cyclones, by comparing two dates, pre-event and post-event. 
The CVA is calculated on the 3 indices: NDVI, BIGR, and NDWIGAO. The magnitude, direction and midpoint of the vector are then studied to detect the presence of a change (threshold relative to the magnitude, if its value is low the vector is not considered a change) and its nature. 
Reference classes were determined during the project, and classification is carried out according to 256 possible classes of change, before being simplified into 9 main classes. 


Installation
============

Download Sen2change and install the python dependencies : 

.. code-block:: bash

   git clone https://framagit.org/espace-dev/sen2change.git
   cd sen2change
   pip3 install --user -e
   
Once installed, you must modify the configuration file to indicate the correct paths in ``~/sen2change_data/config/config.cfg``.
You have to indicate the ``cva_changelib_path`` wich is a set of data listing possible changes between the two L2A product. One csv file used for the project is available in ``sen2change/parameters`` 


Utilisation
============

Then you can load CVAProduct and create an identifier with the names of the two L2A images you wish to compare. 

.. code-block:: python

   from sen2change import CVAProduct
   identifier = "S2A_MSIL2A_20241111T063511_N0511_R134_T40KCB_20241111T075926-S2B_MSIL2A_20241116T063509_N0511_R134_T40KCB_20241116T074641"
   cva = CVAProduct(identifier)


``cva.initialize()`` alllows you to initialize various products if missing from your data (indices, cloud masks) using the sen2chain tool (documentation `here <https://sen2chain.readthedocs.io>`_). 


.. code-block:: python

   cva.initialize() #cm_version = 'CM004', default
   cva.computeCVA() #CVA calculation 
   cva.computeClassif() #Image classification
   cva.computeSeuillageMagnitude() #Masking low magnitudes 
   cva.computeCloudMaskClassif() #Mask classification with cloud mask  
   cva.computeReclassif() #Simplified classification

The final product is a classification indicating if a change happened between the two images and the nature of it (example : Veg-> Water)


Contributing
============

- Pascal Mouquet
- Cyprien Alexandre
- Christophe Revillion
- Julie Mollies
- Thibault Catry
- Gwenaëlle Pennober

This project is open to anyone willing to help. You can contribute by adding new features, reporting and fixing bugs, or propose new ideas.


.. image:: docs/logos_sen2change.png
    :align: center
    :alt: logos


License
============

GPLv3+
