# -*- coding: utf-8 -*-

"""
Module for collecting configuration data from ``~/sen2change_data/config/config.cfg``
"""

import os
import logging
from pathlib import Path
from configparser import ConfigParser


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

ROOT = Path(os.path.realpath(__file__)).parent.parent

SHARED_DATA = dict(
    qml_500classes = ROOT / "parameters" / "test_CVA3D_500classes_black.qml", 
    qml_9classes = ROOT / "parameters" / "9_classes.qml", 
    qml_11classes = ROOT / "parameters" / "11_classes.qml"
    #~ tiles_index_dict=ROOT / "sen2chain" / "data" / "tiles_index_dict.p",
    #~ peps_download=ROOT / "sen2chain" / "peps_download3.py"
    )


class Config:
    """Class for loading, checking, and retrieving configuration parameters.
    The class reads during its first initialization the configurable settings
    from the configuration file.

    Usage::
        >>> Config().get("l1c_path")
    """
    # TODO: Implement the Config class as a singleton.

    _USER_DIR = Path.home() / "sen2change_data"
    _CONFIG_DIR = _USER_DIR / "config"
    _DEFAULT_DATA_DIR = _USER_DIR / "data"
    _CONFIG_FILE = _CONFIG_DIR / "config.cfg"
    #~ _TILES_TO_WATCH = _CONFIG_DIR / "tiles_to_watch.csv"

    def __init__(self) -> None:

        self._config_params = ConfigParser()
        self._config_params["DATA PATHS"] = {"temp_path": "",
                                             "l1c_path": "/DATA_S2/S2_L1C_NEW/",
                                             "l2a_path": "/DATA_SEN2COR/S2_L2A_SEN2COR/",
                                             "indices_path": "/DATA_SEN2COR/S2_INDICES_SEN2COR/",
                                             "time_series_path": "",
                                             "cva_path": "/DATA_S2/S2_CVA_SEN2CHANGE/",
                                             "temporal_summaries_path": "",
                                             "cva_changelib_path": ""}
        #~ self._config_params["SEN2COR PATH"] = {"sen2cor_bashrc_path": ""}
        #~ self._config_params["HUBS LOGINS"] = {"scihub_id": "",
                                              #~ "scihub_pwd": "",
                                              #~ "peps_config_path": ""}
        #~ self._config_params["PROXY SETTINGS"] = {"proxy_http_url": "",
                                                 #~ "proxy_https_url": ""}
        
        if self._CONFIG_FILE.exists():
            self._config_params.read(str(self._CONFIG_FILE))
            self._config_params_disk = ConfigParser()
            self._config_params_disk.read(str(self._CONFIG_FILE))
            if self._config_params_disk != self._config_params: 
                self._create_config()
        else:
            self._create_config()
        
        self.config_dict = dict()

        for section in self._config_params.sections():
            for key in self._config_params[section].keys():
                self.config_dict[key] = self._config_params[section][key]

        self._check_data_paths()

    def _create_config(self) -> None:
        """Create a new config file in ``~/senchange_data/config/config.cfg``."""
        self._USER_DIR.mkdir(exist_ok=True)
        self._CONFIG_DIR.mkdir(exist_ok=True)
        self._DEFAULT_DATA_DIR.mkdir(exist_ok=True)
        
        with open(str(self._CONFIG_FILE), "w") as cfg_file:
            self._config_params.write(cfg_file)

    def _check_data_paths(self) -> None:
        """
        Checks if data paths are provided and valids. If not, create default
        folders in sen2change_data/data and update the configuration file.
        """
        def update_config(section, key, val):
            """
            Update a setting in config.ini
            """
            self._config_params.set(section, key, val)
            with open(str(self._CONFIG_FILE), "w") as cfg_file:
                self._config_params.write(cfg_file)

        data_paths = [option for option in self._config_params["DATA PATHS"]]

        for path in data_paths:
            value = self.config_dict[path]

            if value.rstrip() == "" or not Path(value).exists():

                default_value = self._DEFAULT_DATA_DIR / path.replace("_path", "").upper()
                default_value.mkdir(parents=True, exist_ok=True)
                update_config("DATA PATHS", path, str(default_value))

                logger.info("{}: using default at {}".format(path, str(default_value)))

        #~ sen2cor_bashrc_path_value = self.config_dict["sen2cor_bashrc_path"]

        #~ if sen2cor_bashrc_path_value.rstrip() == "" or not Path(sen2cor_bashrc_path_value).exists():
            #~ logging.error("Make sure the path to the sen2cor Bashrc file is valid.")
            #~ raise ValueError("Invalid sen2cor Bashrc")

    def get(self, param: str) -> str:
        """Returns a parameter value.

        :param param: parameter.
        """
        if param not in self.config_dict:
            raise ValueError("Invalid parameter", param)

        return self.config_dict.get(param)

    #~ @property
    #~ def tiles_to_watch(self):
        #~ return self._TILES_TO_WATCH
