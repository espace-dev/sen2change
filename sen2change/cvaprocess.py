#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for computing Change Vector Analysis
"""

import logging
from pathlib import Path, PosixPath
import rasterio
import datetime
import numpy as np
import sys
from rasterio.warp import reproject, Resampling
from typing import Union
import os
from sen2chain import Tile, L2aProduct

from .cvaconfig import Config
#~ from .cvagestion import CVAProduct

from osgeo import gdal

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def tiff_2_jp2(img_path: Union[str, PosixPath],
        out_path: Union[str, PosixPath]="./file_tif2jp2.jp2") -> PosixPath:
    """
    Convert a file from TIF to JP2.
    :param out_path: path to the output raster.
    """
    logger.info("converting TIFF file to JPEG2000")
    src_ds = gdal.Open(str(img_path))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    for i in range(src_ds.RasterCount):
            src_ds.GetRasterBand(i+1).SetNoDataValue(float(16383))
    dst_ds = driver.CreateCopy(str(out_path), src_ds, options=['NBITS=15', 'CODEC=JP2', 'QUALITY=20'])
    
    #~ dst_ds = driver.CreateCopy(str(out_path), src_ds,
                               #~ options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
    
    dst_ds = None
    src_ds = None
    return str(Path(str(out_path)).absolute)


def rasterCVA(tile : Tile = None,
              date_bef: str = None,
              date_aft: str = None):
                  
    
    try:
        tile = Tile(tile)
    except:
        logger.info("Parameter tile not provided or invalid ex: 40KCB")
        
    try:
        datetime_bef = datetime.datetime.strptime(date_bef, '%Y-%m-%d')
    except:
        logger.info("Parameter date_def not provided or invalid 'YYYY-MM-DD'")
    try:        
        datetime_aft = datetime.datetime.strptime(date_aft, '%Y-%m-%d')
    except:
        logger.info("Parameter date_def not provided or invalid 'YYYY-MM-DD'")
    
    try:
        ndvi_bef_product = tile.ndvi.raws.filter_dates(date_min=date_bef, date_max=date_aft).products[0]
        ndvi_bef_path = tile._paths['indices']['ndvi'] / ndvi_bef_product[:-9] / ndvi_bef_product
        #~ logger.info(ndvi_bef_path)
    except:
        logger.error("No NDVI for this date {}".format(date_bef))
        sys.exit(1)

    try:
        ndvi_aft_product = tile.ndvi.raws.filter_dates(date_min=date_aft, date_max=date_aft).products[0]
        ndvi_aft_path = tile._paths['indices']['ndvi'] / ndvi_aft_product[:-9] / ndvi_aft_product
        #~ logger.info(ndvi_aft_path)
    except:
        logger.error("No NDVI for this date {}".format(date_aft))
        sys.exit(1)
    
    try:
        bigr_bef_product = tile.bigr.raws.filter_dates(date_min=date_bef, date_max=date_bef).products[0]
        bigr_bef_path = tile._paths['indices']['bigr'] / bigr_bef_product[:-9] / bigr_bef_product
        #~ logger.info(bigr_bef_path)
    except:
        logger.error("No BIGR for this date {}".format(date_bef))
        sys.exit(1)
    
    try:
        bigr_aft_product = tile.bigr.raws.filter_dates(date_min=date_aft, date_max=date_aft).products[0]
        bigr_aft_path = tile._paths['indices']['bigr'] / bigr_aft_product[:-9] / bigr_aft_product
        #~ logger.info(bigr_aft_path)
    except:
        logger.error("No BIGR for this date {}".format(date_aft))
        sys.exit(1)
        
    out_folder = Config().get("cva_path")
    out_path = Path(out_folder) / tile.name / (date_bef + '_' + date_aft) / (tile.name + "_CVA_" + '_' + date_bef + '_' + date_aft + ".tif")
        
    with rasterio.open(str(ndvi_bef_path)) as ndvi_bef_src, \
         rasterio.open(str(ndvi_aft_path)) as ndvi_aft_src, \
         rasterio.open(str(bigr_bef_path)) as bigr_bef_src, \
         rasterio.open(str(bigr_aft_path)) as bigr_aft_src:
        
        ndvi_bef_profile = ndvi_bef_src.profile
        #~ # swir_profile = swir_src.profile
        
        ndvi_bef = ndvi_bef_src.read(1).astype(np.float32)
        ndvi_aft = ndvi_aft_src.read(1).astype(np.float32)
        ndvi_diff = 0.5*(ndvi_aft - ndvi_bef)
        
        
        bigr_bef = bigr_bef_src.read(1).astype(np.float32)
        bigr_aft = bigr_aft_src.read(1).astype(np.float32)
        bigr_diff = bigr_aft - bigr_bef
        
        magnitude = np.sqrt(ndvi_diff*ndvi_diff + bigr_diff*bigr_diff)
        logger.info("Magnitude: {}".format(magnitude.dtype))
        angle = np.arctan2(ndvi_diff, bigr_diff)* (180 / np.pi)
        logger.info("angle: {}".format(angle.dtype))
        angle = np.where(angle < 0, 360 + angle, angle)
        logger.info("angle: {}".format(angle.dtype))
        transp = np.copy(magnitude)
        logger.info("transp: {}".format(transp.dtype))
        #~ transp = np.where(magnitude < 5000, 255*(magnitude/5000), 255)
        transp = np.clip(transp*(255.0/3000), 0, 255).astype(np.float32)
        #~ transp *= 255.0/5000
        logger.info("transp: {}".format(transp.dtype))
        
    ndvi_bef_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.float32,
                       nodata=32767,
                       transform=ndvi_bef_src.transform,
                       count=3)
                            
    ndvi_bef_profile.pop('tiled', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **ndvi_bef_profile) as dst:
            dst.write(magnitude, 1)
            dst.write(angle, 2)
            dst.write(transp, 3)



def rasterCVAbands(tile : Tile = None,
              date_bef: str = None,
              date_aft: str = None,
              bandx: str = None,
              bandy:str = None):
                  
    
    try:
        tile = Tile(tile)
    except:
        logger.info("Parameter tile not provided or invalid ex: 40KCB")
        
    try:
        datetime_bef = datetime.datetime.strptime(date_bef, '%Y-%m-%d')
    except:
        logger.info("Parameter date_def not provided or invalid 'YYYY-MM-DD'")
    try:        
        datetime_aft = datetime.datetime.strptime(date_aft, '%Y-%m-%d')
    except:
        logger.info("Parameter date_def not provided or invalid 'YYYY-MM-DD'")
    
    try:
        bef_l2a = tile.l2a.filter_dates(date_min=date_bef, date_max=date_bef).products[0]
        #~ bef_path = tile._paths['indices']['ndvi'] / ndvi_bef_product[:-9] / ndvi_bef_product
        logger.info("Before event product : "+ bef_l2a)
    except:
        logger.info("No product for this date {}".format(date_bef))
        #~ sys.exit(1)
    try:
        bef_product =  L2aProduct(bef_l2a)
        logger.info("Before product Ok")
    except:
        logger.info("Before product conversion failed")       
    try:
        aft_l2a = tile.l2a.filter_dates(date_min=date_aft, date_max=date_aft).products[0]
        #~ bef_path = tile._paths['indices']['ndvi'] / ndvi_bef_product[:-9] / ndvi_bef_product
        logger.info("After event product : "+ aft_l2a)
    except:
        logger.info("No product for this date {}".format(date_aft))
        #~ sys.exit(1)
	
    try:
        aft_product =  L2aProduct(aft_l2a)
        logger.info("After product Ok")
    except:
        logger.info("After product conversion failed")
    
    
    try:
        bef_bandx = getattr(bef_product,bandx)
        aft_bandx = getattr(aft_product,bandx)
        logger.info(bef_bandx)
    except:
        logger.info("No band "+ bandx +" for this product. ex 'b02_10m'.")
    
    try:
        bef_bandy = getattr(bef_product,bandy)
        aft_bandy = getattr(aft_product,bandy)
        logger.info(bef_bandx)
    except:
        logger.info("No band "+ bandy +" for this product. ex 'b02_10m'.")
    
    with rasterio.open(str(bef_bandx)) as bandx_bef_src, \
         rasterio.open(str(aft_bandx)) as bandx_aft_src, \
         rasterio.open(str(bef_bandy)) as bandy_bef_src, \
         rasterio.open(str(aft_bandy)) as bandy_aft_src:      
        
          
        out_folder = Config().get("cva_path")
        out_path = Path(out_folder) / tile.name /  (date_bef + '_' + date_aft) / (tile.name + '_' + date_bef + '_' + date_aft + '_' + bandx + '_' + bandy + ".tif")
            
        bandx_bef = bandx_bef_src.read(1).astype(np.float32)
        bandx_aft = bandx_aft_src.read(1).astype(np.float32)
       
        bandy_bef = bandy_bef_src.read(1).astype(np.float32)
        bandy_aft = bandy_aft_src.read(1).astype(np.float32)
       
        cva_profile = bandx_bef_src.profile
       
        cva_profile.update(driver="Gtiff",
                           compress="NONE",
                           tiled=False,
                           dtype=np.float32,
                           nodata=32767,
                           transform=bandx_bef_src.transform,
                           count=3)
                                
        cva_profile.pop('tiled', None)
        if bandx[4:6] != bandy[4:6]: 
            logger.info("bx != by")
            if bandx [4:6] != "10":
                 # reproject bandx to bandy resolution (10m) if different
                bandx_bef_reproj = np.empty(bandy_bef.shape, dtype=np.float32)
                reproject(source=bandx_bef,
                          destination=bandx_bef_reproj,
                          src_transform=bandx_bef_src.transform,
                          src_crs=bandx_bef_src.crs,
                          dst_transform=bandy_bef_src.transform,
                          dst_crs=bandy_bef_src.crs,
                          resampling=Resampling.bilinear)
                bandx_bef = bandx_bef_reproj
                logger.info("bandx before resampled")
                # reproject bandx to bandy resolution (10m) if different
                bandx_aft_reproj = np.empty(bandy_aft.shape, dtype=np.float32)
                reproject(source=bandx_aft,
                          destination=bandx_aft_reproj,
                          src_transform=bandx_aft_src.transform,
                          src_crs=bandx_aft_src.crs,
                          dst_transform=bandy_aft_src.transform,
                          dst_crs=bandy_aft_src.crs,
                          resampling=Resampling.bilinear)
                bandx_aft = bandx_aft_reproj
                # if bandx != 10m update profile with bandy 10m
                cva_profile = bandy_bef_src.profile
       
                cva_profile.update(driver="Gtiff",
                           compress="NONE",
                           tiled=False,
                           dtype=np.float32,
                           nodata=32767,
                           transform=bandy_bef_src.transform,
                           count=3)
                logger.info("bandx after resampled")
            if bandy [4:6] != "10":
                  # reproject bandy to bandx resolution (10m) if different
                bandy_bef_reproj = np.empty(bandx_bef.shape, dtype=np.float32)
                reproject(source=bandy_bef,
                          destination=bandy_bef_reproj,
                          src_transform=bandy_bef_src.transform,
                          src_crs=bandy_bef_src.crs,
                          dst_transform=bandx_bef_src.transform,
                          dst_crs=bandx_bef_src.crs,
                          resampling=Resampling.bilinear)
                bandy_bef = bandy_bef_reproj
                logger.info("bandy before resampled")
                # reproject bandy to bandx resolution (10m) if different
                bandy_aft_reproj = np.empty(bandx_aft.shape, dtype=np.float32)
                reproject(source=bandy_aft,
                          destination=bandy_aft_reproj,
                          src_transform=bandy_aft_src.transform,
                          src_crs=bandy_aft_src.crs,
                          dst_transform=bandx_aft_src.transform,
                          dst_crs=bandx_aft_src.crs,
                          resampling=Resampling.bilinear)
                bandy_aft = bandy_aft_reproj
                # if bandy != 10m update profile with bandx 10m
                cva_profile = bandx_bef_src.profile
       
                cva_profile.update(driver="Gtiff",
                           compress="NONE",
                           tiled=False,
                           dtype=np.float32,
                           nodata=32767,
                           transform=bandx_bef_src.transform,
                           count=3)
                logger.info("bandy after resampled")
                
        bandx_diff = bandx_bef - bandx_aft
        bandy_diff = bandy_bef - bandy_aft
        
        magnitude = np.sqrt(bandx_diff*bandx_diff + bandy_diff*bandy_diff)
        angle = np.arctan2(bandx_diff, bandy_diff)* 180 / np.pi
        angle_0_360 = np.where(angle < 0, 360 + angle, angle)
        transp = np.copy(magnitude)
        transp = np.where(transp < 5000, 255*(transp/5000), 255)
        
        
        with rasterio.Env(GDAL_CACHEMAX=512) as env:
            with rasterio.open(str(out_path), "w", **cva_profile) as dst:
                dst.write(magnitude, 1)
                dst.write(angle_0_360, 2)
                dst.write(transp, 3)
                
        logger.info("rasterCVAbands DONE")


def rasterCVA3D(tile : Tile = None, 
                indices: list = None,
                date_bef: str = None,
                date_aft: str = None):
                  
    try:
        tile = Tile(tile)
    except:
        logger.info("Parameter tile not provided or invalid ex: 40KCB")
    try:
        datetime_bef = datetime.datetime.strptime(date_bef, '%Y-%m-%d')
    except:
        logger.info("Parameter date_def not provided or invalid 'YYYY-MM-DD'")
    try:        
        datetime_aft = datetime.datetime.strptime(date_aft, '%Y-%m-%d')
    except:
        logger.info("Parameter date_def not provided or invalid 'YYYY-MM-DD'")
    if not indices:
        logger.error("Parameter indices not provided {}".format())
        sys.exit(1)
    else:
        x, y, z = indices
        x = x.lower()
        y = y.lower()
        z = z.lower()

    # Recuperation des path before & after dans la dimension 1 (x)
    try:
        x_bef_product = getattr(tile, x).raws.filter_dates(date_min=date_bef, date_max=date_bef).products[0]
        x_bef_path = tile._paths['indices'][x] / x_bef_product[:-5-len(x)] / x_bef_product
        #~ logger.info(x_bef_path)
    except:
        logger.error("No {} for this date {}".format(x, date_bef))
        sys.exit(1)

    try:
        x_aft_product = getattr(tile, x).raws.filter_dates(date_min=date_aft, date_max=date_aft).products[0]
        x_aft_path = tile._paths['indices'][x] / x_aft_product[:-5-len(x)] / x_aft_product
        #~ logger.info(x_aft_path)
    except:
        logger.error("No {} for this date {}".format(x, date_aft))
        sys.exit(1)
        
    # Recuperation des path before & after dans la dimension 2 (y)
    try:
        y_bef_product = getattr(tile, y).raws.filter_dates(date_min=date_bef, date_max=date_bef).products[0]
        y_bef_path = tile._paths['indices'][y] / y_bef_product[:-5-len(y)] / y_bef_product
        #~ logger.info(y_bef_path)
    except:
        logger.error("No {} for this date {}".format(y, date_bef))
        sys.exit(1)
        
    try:
        y_aft_product = getattr(tile, y).raws.filter_dates(date_min=date_aft, date_max=date_aft).products[0]
        y_aft_path = tile._paths['indices'][y] / y_aft_product[:-5-len(y)] / y_aft_product
        #~ logger.info(y_aft_path)
    except:
        logger.error("No {} for this date {}".format(y, date_aft))
        sys.exit(1)
        
    # Recuperation des path before & after dans la dimension 3 (z)
    try:
        z_bef_product = getattr(tile, z).raws.filter_dates(date_min=date_bef, date_max=date_bef).products[0]
        z_bef_path = tile._paths['indices'][z] / z_bef_product[:-5-len(z)] / z_bef_product
        #~ logger.info(z_bef_path)
    except:
        logger.error("No {} for this date {}".format(z, date_bef))
        sys.exit(1)
        
    try:
        z_aft_product = getattr(tile, z).raws.filter_dates(date_min=date_aft, date_max=date_aft).products[0]
        z_aft_path = tile._paths['indices'][z] / z_aft_product[:-5-len(z)] / z_aft_product
        #~ logger.info(z_aft_path)
    except:
        logger.error("No {} for this date {}".format(z, date_aft))
        sys.exit(1)
        
    #~ z_bef_path = "/DATA_S2/TEMP/20171225_GBNDVI.tif"
    #~ z_aft_path = "/DATA_S2/TEMP/20180129_GBNDVI.tif"
        
    #~ out_folder = Path(Config().get("cva_path")) / tile.name / (date_bef + '_' + date_aft)
    
    out_folder = Path(Config().get("cva_path")) / tile.name / (z_bef_product[:-5-len(z)] + '-' + z_aft_product[:-5-len(z)])
    
    # je sais pas à quoi sert la ligne du dessous (à virer ?)
    #~ '_'.join(t.ndvi.raws.products[0][:-9].split('_')[i] for i in [2,6])

    
    logger.info(out_folder)
    out_folder.mkdir(parents = True, exist_ok = True)
    out_path_tif = Path(out_folder) / (tile.name + "_CVA3D_" + x.upper() + '-' + y.upper() + '-' + z.upper() + '_' + date_bef + '_' + date_aft + ".tif")
    out_path_jp2 = Path(out_folder) / (tile.name + "_CVA3D_" + x.upper() + '-' + y.upper() + '-' + z.upper() + '_' + date_bef + '_' + date_aft + ".jp2")
    
    with rasterio.open(str(x_bef_path)) as x_bef_src, \
         rasterio.open(str(x_aft_path)) as x_aft_src, \
         rasterio.open(str(y_bef_path)) as y_bef_src, \
         rasterio.open(str(y_aft_path)) as y_aft_src, \
         rasterio.open(str(z_aft_path)) as z_aft_src, \
         rasterio.open(str(z_bef_path)) as z_bef_src:
        
        x_bef_profile = x_bef_src.profile
        
        a=[1] * 3
        b=[0] * 3
        for index, value in enumerate(indices):
            if 'bi' in value.lower():
                a[index] = 2
                b[index] = -5000
        
        logger.info("Calcul du x_diff...")
        x_bef = a[0]*(x_bef_src.read(1).astype(np.float32)+b[0])
        x_aft = a[0]*(x_aft_src.read(1).astype(np.float32)+b[0])
        x_diff = x_aft-x_bef
        
        logger.info("Calcul du y_diff...")
        y_bef = a[1]*(y_bef_src.read(1).astype(np.float32)+b[1])
        y_aft = a[1]*(y_aft_src.read(1).astype(np.float32)+b[1])
        y_diff = y_aft-y_bef
        
        logger.info("Calcul du z_diff...")
        z_bef = a[2]*(z_bef_src.read(1).astype(np.float32)+b[2])
        z_aft = a[2]*(z_aft_src.read(1).astype(np.float32)+b[2])
        z_diff = z_aft-z_bef
        
        logger.info("Calcul de la magnitude...")
        magnitude = np.sqrt(x_diff * x_diff + y_diff * y_diff + z_diff * z_diff).astype(np.int16)
        
        logger.info("Calcul des cos...")
        cosx = (10000 * x_diff / magnitude).astype(np.int16)
        cosy = (10000 * y_diff / magnitude).astype(np.int16)
        cosz = (10000 * z_diff / magnitude).astype(np.int16)
        
        logger.info("Calcul de la mediane...")
        x_mean = ((x_aft+x_bef)/2)
        y_mean = ((y_aft+y_bef)/2)
        z_mean = ((z_aft+z_bef)/2)
        
        magnitude_mean = np.sqrt(x_mean * x_mean + y_mean * y_mean + z_mean * z_mean).astype(np.int16)
        
        cosx_mean = (10000 * x_mean / magnitude_mean).astype(np.int16)
        cosy_mean = (10000 * y_mean / magnitude_mean).astype(np.int16)
        cosz_mean = (10000 * z_mean / magnitude_mean).astype(np.int16)
        
    x_bef_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.int16,
                       nodata=32767,
                       transform=x_bef_src.transform,
                       count=11)
                            
    x_bef_profile.pop('tiled', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_tif), "w", **x_bef_profile) as dst:
            dst.write(magnitude, 1)
            dst.write(cosx, 2)
            dst.write(cosy, 3)
            dst.write(cosz, 4)
            dst.write(x_mean.astype(np.int16), 5)
            dst.write(y_mean.astype(np.int16), 6)
            dst.write(z_mean.astype(np.int16), 7)
            dst.write(magnitude_mean, 8)
            dst.write(cosx_mean, 9)
            dst.write(cosy_mean, 10)
            dst.write(cosz_mean, 11)

                
    tiff_2_jp2(out_path_tif, out_path_jp2)
    os.remove(str(out_path_tif))


def computeCVA(cva_prod = None): 
                  
    tile = Tile(cva_prod.tile)
    
    x_bef_product = cva_prod.l2a_id_bef + "_NDVI.jp2"
    x_bef_path = tile._paths["indices"]["ndvi"] / cva_prod.l2a_id_bef / x_bef_product
    x_aft_product = cva_prod.l2a_id_aft + "_NDVI.jp2"
    x_aft_path = tile._paths["indices"]["ndvi"] / cva_prod.l2a_id_aft / x_aft_product
    
    y_bef_product = cva_prod.l2a_id_bef + "_BIGR.jp2"
    y_bef_path = tile._paths["indices"]["bigr"] / cva_prod.l2a_id_bef / y_bef_product
    y_aft_product = cva_prod.l2a_id_aft + "_BIGR.jp2"
    y_aft_path = tile._paths["indices"]["bigr"] / cva_prod.l2a_id_aft / y_aft_product

    z_bef_product = cva_prod.l2a_id_bef + "_NDWIGAO.jp2"
    z_bef_path = tile._paths["indices"]["ndwigao"] / cva_prod.l2a_id_bef / z_bef_product
    z_aft_product = cva_prod.l2a_id_aft + "_NDWIGAO.jp2"
    z_aft_path = tile._paths["indices"]["ndwigao"] / cva_prod.l2a_id_aft / z_aft_product
    
    out_folder = cva_prod.path
    
    out_path_tif = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_cva.tif")
    out_path_jp2 = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_cva.jp2")
    out_path_jp2_aux = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_cva.jp2.aux.xml")

    out_path_magn_tif = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_magnitude.tif")
    out_path_magn_jp2 = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_magnitude.jp2")
    
    with rasterio.open(str(x_bef_path)) as x_bef_src, \
         rasterio.open(str(x_aft_path)) as x_aft_src, \
         rasterio.open(str(y_bef_path)) as y_bef_src, \
         rasterio.open(str(y_aft_path)) as y_aft_src, \
         rasterio.open(str(z_aft_path)) as z_aft_src, \
         rasterio.open(str(z_bef_path)) as z_bef_src:
        
        x_bef_profile = x_bef_src.profile
        
        a=[1] * 3
        b=[0] * 3
        a[1] = 2
        b[1] = -5000
        
        logger.info("Calcul du x_diff...")
        x_bef = a[0]*(x_bef_src.read(1).astype(np.float32)+b[0])
        x_aft = a[0]*(x_aft_src.read(1).astype(np.float32)+b[0])
        x_diff = x_aft-x_bef
        
        logger.info("Calcul du y_diff...")
        y_bef = a[1]*(y_bef_src.read(1).astype(np.float32)+b[1])
        y_aft = a[1]*(y_aft_src.read(1).astype(np.float32)+b[1])
        y_diff = y_aft-y_bef
        
        logger.info("Calcul du z_diff...")
        z_bef = a[2]*(z_bef_src.read(1).astype(np.float32)+b[2])
        z_aft = a[2]*(z_aft_src.read(1).astype(np.float32)+b[2])
        z_diff = z_aft-z_bef
        
        logger.info("Calcul de la magnitude...")
        magnitude = np.sqrt(x_diff * x_diff + y_diff * y_diff + z_diff * z_diff).astype(np.int16)
        
        logger.info("Calcul des cos...")
        cosx = (10000 * x_diff / magnitude).astype(np.int16)
        cosy = (10000 * y_diff / magnitude).astype(np.int16)
        cosz = (10000 * z_diff / magnitude).astype(np.int16)
        
        logger.info("Calcul de la mediane...")
        x_mean = ((x_aft+x_bef)/2)
        y_mean = ((y_aft+y_bef)/2)
        z_mean = ((z_aft+z_bef)/2)
        
        magnitude_mean = np.sqrt(x_mean * x_mean + y_mean * y_mean + z_mean * z_mean).astype(np.int16)
        
        cosx_mean = (10000 * x_mean / magnitude_mean).astype(np.int16)
        cosy_mean = (10000 * y_mean / magnitude_mean).astype(np.int16)
        cosz_mean = (10000 * z_mean / magnitude_mean).astype(np.int16)
        
    x_bef_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.int16,
                       nodata=32767,
                       transform=x_bef_src.transform,
                       count=10)
    x_bef_profile.pop('tiled', None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_tif), "w", **x_bef_profile) as dst:
            dst.write(cosx, 1)
            dst.write(cosy, 2)
            dst.write(cosz, 3)
            dst.write(x_mean.astype(np.int16), 4)
            dst.write(y_mean.astype(np.int16), 5)
            dst.write(z_mean.astype(np.int16), 6)
            dst.write(magnitude_mean, 7)
            dst.write(cosx_mean, 8)
            dst.write(cosy_mean, 9)
            dst.write(cosz_mean, 10)

    x_bef_profile.update(count=1)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_magn_tif), "w", **x_bef_profile) as dst:
            dst.write(magnitude, 1)
    tiff_2_jp2(out_path_magn_tif, out_path_magn_jp2)
    os.remove(str(out_path_magn_tif))
    
    if out_path_jp2.exists():
        out_path_jp2.unlink()
    if out_path_jp2_aux.exists():
        out_path_jp2_aux.unlink()
