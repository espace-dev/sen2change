#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for computing Change Vector Analysis
"""

import logging
import fiona
from rasterstats import zonal_stats
from collections import OrderedDict
from typing import Dict
import re
import pandas as pd
import numpy as np
import itertools
from collections import OrderedDict
import rasterio
import pathlib
import time
from datetime import timedelta
from osgeo import gdal
import os
import fileinput
import sys
import shutil



from sen2chain import Tile, L2aProduct, TimeSeries
from .cvaconfig import Config, SHARED_DATA

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_raster_stats_in_geom(
            feature: Dict,
            raster_path: str
            ) -> Dict:
    """Extracts statistics from a raster in a geometry.

    :param feature: GeoJSON like object.
    :param raster_path: path to the raster.
    """
    geom = shape(transform.transform_geom("EPSG:4326",
                                          rasterio.open(str(raster_path)).crs["init"],
                                          feature["geometry"]))

    stats = zonal_stats(geom, str(raster_path), stats=["count",
                                                        "nodata",
                                                        "min",
                                                        "max",
                                                        "median",
                                                        "mean",
                                                        "std"])[0]
    return stats


"""
Debut de code pour extraire les valeurs du raster dans un polygone
pour faire des stats sur les magnitudes des vecteurs de CVA
https://gis.stackexchange.com/questions/260304/extract-raster-values-within-shapefile-with-pygeoprocessing-or-gdal

import rasterio
from rasterio.mask import mask
import geopandas as gpd
shapefile = gpd.read_file("extraction.shp")
# extract the geometry in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
geometry = geoms[0] # shapely geometry
# transform to GeJSON format
from shapely.geometry import mapping
geoms = [mapping(geoms[0])]
# extract the raster values values within the polygon 
with rasterio.open("raster.tif") as src:
     out_image, out_transform = mask(src, geoms, crop=True)

The out_image result is a Numpy masked array

# no data values of the original raster
no_data=src.nodata
print no_data
-9999.0
# extract the values of the masked array
data = out_image.data[0]
# extract the row, columns of the valid values
import numpy as np
row, col = np.where(data != no_data) 
elev = np.extract(data != no_data, data)
"""

class BuildCVARef:
    """Class building change library.

    :param vector_file:
    :param indices:
    
    Usage:
        >>> change = CVAClassif('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/ROI_20171225_WGS84.shp', 
                                         ['NDVI', 'BIGR', 'NDWIGAO'])
    """
    def __init__(
            self,
            vector_file: str,
            indices: [str] = None
    ) -> None:

        self._vector_file = vector_file
        self._indices = indices
        self.values = None
        self._extract_classes_values()
        self.grouped = self.values.groupby(['Class', 'indice']).apply(self._my_agg).reindex(level=1, labels=self._indices)

        self.means = self.grouped['weighted_mean'].unstack(level=[1]).reindex(columns=(self._indices), copy = False)
        self.std = self.grouped['pooled_std'].unstack(level=[1]).reindex(columns=(self._indices), copy = False)
        #~ self.means = self.means.reindex(columns=(self._indices), copy = False)
        self.changelib = None
        self._compute_changes_lib()
        self.changelib1std = None
        self.changelib2std = None
        self.changelib3std = None
        self._compute_changes_lib_std()

    def _extract_classes_values(self):
        """
        Get values
        :param out_path: path to the output raster.
        
        Usage:
        a = sen2change.extract_classes_values('/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/ROI_20171225_WGS84.shp', 
                                             ['NDVI', 'BIGR', 'NDWIGAO'])
        """
        
        #~ df_dicts = dict()
        rows_list = []
        
        with fiona.open(str(self._vector_file), "r") as vectors:
            features = {feat["id"]: feat for feat in vectors}
            for index, fid in enumerate(features):
                identifier = features[fid]['properties']['L2A']
                tile = re.findall("^.+_T([0-9]{2}[A-Z]{3})", identifier)
                date = re.findall("^.+_.+_([0-9]{8})T", identifier)
                tile = Tile(tile[0])
                
                            
                for index2, indice in enumerate(self._indices):
                    #~ logger.info("computing {}/{}: {}".format(index2 + 1, len(indices), indice))
                    dict_values = OrderedDict()
                    dict_values["fid"] = fid
                    dict_values["indice"] = indice
                    product_path = tile._paths['indices'][indice.lower()] / identifier[:-5] / (identifier[:-5] + '_' + indice.upper() + '.jp2')
                    #~ logger.info(product_path)
                     
                    dict_values.update(TimeSeries._get_raster_stats_in_geom(features[fid], product_path))
                    
                    for prop in features[fid]["properties"]:
                        dict_values[prop] = features[fid]["properties"][prop]
                    
                    rows_list.append(dict_values)
                    #~ if rows_list:
                        #~ df_dicts[indice] = pd.DataFrame.from_dict(rows_list)
    
        self.values = pd.DataFrame.from_dict(rows_list)
    
    def _my_agg(self, x):
        """
        Function to compute df stats
        """
        names = {'weighted_mean': (x['mean'] * x['count']).sum()/x['count'].sum(), 
                 'pooled_std': np.sqrt(((x['count']-1)*x['std']*x['std']).sum()/(x['count'].sum()-x['std'].count())),
                 'sum_count': x['count'].sum(), 
                 'global_min': x['min'].min(), 
                 'global_max': x['max'].max(),
                 'nb_polygons': x['std'].count()}
        return pd.Series(names, index=['weighted_mean', 'pooled_std', 'sum_count', 'global_min', 'global_max', 'nb_polygons'])
        
    def _compute_changes_lib(self):
        """

        """
        df = self.means
        changes = OrderedDict()
        changes['ChangeClass']= [a for a in (e + '->' + f for e, f in list(itertools.permutations(df.index,2)))]
        for i in df.columns:
            changes[i+'_diff'] = [a for a in (f-e for e, f in list(itertools.permutations(df[i],2)))]
            changes[i+'_mean'] = [a for a in ((f+e)/2 for e, f in list(itertools.permutations(df[i],2)))]
        toto = pd.DataFrame.from_dict(changes)
        toto['BIGR_diff'] = toto['BIGR_diff'] * 2
        toto['BIGR_mean'] = (toto['BIGR_mean']-5000) * 2
        toto.set_index('ChangeClass', inplace = True)
        toto['magnitude_diff'] = (toto[toto.columns[[0, 2, 4]]].pow(axis=1, other=2).sum(axis=1))**0.5
        for i in toto.columns[[0, 2, 4]]:
            toto['cos_'+i] = toto[i]/toto['magnitude_diff']
        toto['magnitude_mean'] = (toto[toto.columns[[1, 3, 5]]].pow(axis=1, other=2).sum(axis=1))**0.5
        for i in toto.columns[[1, 3, 5]]:
            toto['cos_'+i] = toto[i]/toto['magnitude_mean']
        self.changelib = toto
    
    def _compute_changes_lib_std(self):
        """

        """
        coeffs = (np.array(np.meshgrid([-1, 1], [-1, 1], [-1, 1])).T.reshape(-1,3))*[1,2,1]
        dfm = self.means
        dfs = self.std
        titi = None
        for line in coeffs:
            df = dfm + line * dfs
            #~ logger.info(df)
            changes = OrderedDict()
            changes['ChangeClass']= [a for a in (e + '->' + f for e, f in list(itertools.permutations(df.index,2)))]
            for i in df.columns:
                changes[i+'_diff'] = [a for a in (f-e for e, f in list(itertools.permutations(df[i],2)))]
                #~ changes[i+'_mean'] = [a for a in ((f+e)/2 for e, f in list(itertools.permutations(df[i],2)))]
            toto = pd.DataFrame.from_dict(changes)
            toto['BIGR_diff'] = toto['BIGR_diff'] * 2
            #~ toto['BIGR_mean'] = (toto['BIGR_mean']-5000) * 2
            toto.set_index('ChangeClass', inplace = True)
            toto['magnitude_diff'] = (toto[toto.columns[[0, 1, 2]]].pow(axis=1, other=2).sum(axis=1))**0.5
            if titi is None:
                titi = pd.DataFrame([toto['magnitude_diff']]).transpose()
                titi.rename(columns={'magnitude_diff': str(line)}, inplace=True)
            else:
                titi[str(line)] = toto['magnitude_diff']
        titi['min_mg'] = titi.min(axis=1)
        titi['magnitude_diff'] = self.changelib['magnitude_diff']
        self.changelib1std = titi
                
        tata = None
        for line in coeffs:
            df = dfm + 2* line * dfs
            #~ logger.info(df)
            changes = OrderedDict()
            changes['ChangeClass']= [a for a in (e + '->' + f for e, f in list(itertools.permutations(df.index,2)))]
            for i in df.columns:
                changes[i+'_diff'] = [a for a in (f-e for e, f in list(itertools.permutations(df[i],2)))]
                #~ changes[i+'_mean'] = [a for a in ((f+e)/2 for e, f in list(itertools.permutations(df[i],2)))]
            toto = pd.DataFrame.from_dict(changes)
            toto['BIGR_diff'] = toto['BIGR_diff'] * 2
            #~ toto['BIGR_mean'] = (toto['BIGR_mean']-5000) * 2
            toto.set_index('ChangeClass', inplace = True)
            toto['magnitude_diff'] = (toto[toto.columns[[0, 1, 2]]].pow(axis=1, other=2).sum(axis=1))**0.5
            if tata is None:
                tata = pd.DataFrame([toto['magnitude_diff']]).transpose()
                tata.rename(columns={'magnitude_diff': str(line)}, inplace=True)
            else:
                tata[str(line)] = toto['magnitude_diff']
        tata['min_mg'] = tata.min(axis=1)
        tata['magnitude_diff'] = self.changelib['magnitude_diff']
        self.changelib2std = tata        
        
        tutu = None
        for line in coeffs:
            df = dfm + 3 * line * dfs
            #~ logger.info(df)
            changes = OrderedDict()
            changes['ChangeClass']= [a for a in (e + '->' + f for e, f in list(itertools.permutations(df.index,2)))]
            for i in df.columns:
                changes[i+'_diff'] = [a for a in (f-e for e, f in list(itertools.permutations(df[i],2)))]
                #~ changes[i+'_mean'] = [a for a in ((f+e)/2 for e, f in list(itertools.permutations(df[i],2)))]
            toto = pd.DataFrame.from_dict(changes)
            toto['BIGR_diff'] = toto['BIGR_diff'] * 2
            #~ toto['BIGR_mean'] = (toto['BIGR_mean']-5000) * 2
            toto.set_index('ChangeClass', inplace = True)
            toto['magnitude_diff'] = (toto[toto.columns[[0, 1, 2]]].pow(axis=1, other=2).sum(axis=1))**0.5
            if tutu is None:
                tutu = pd.DataFrame([toto['magnitude_diff']]).transpose()
                tutu.rename(columns={'magnitude_diff': str(line)}, inplace=True)
            else:
                tutu[str(line)] = toto['magnitude_diff']
        tutu['min_mg'] = tutu.min(axis=1)
        tutu['magnitude_diff'] = self.changelib['magnitude_diff']
        self.changelib3std = tutu  

def compute_changes_lib(csv_path: str):
    
    df = pd.read_csv(csv_path)
    df = df.set_index('Class')
    changes = OrderedDict()
    changes['ChangeClass']= [a for a in (e + '->' + f for e, f in list(itertools.permutations(df.index,2)))]
    for i in df.columns:
        changes[i+'_diff'] = [a for a in (f-e for e, f in list(itertools.permutations(df[i],2)))]
        changes[i+'_mean'] = [a for a in ((f+e)/2 for e, f in list(itertools.permutations(df[i],2)))]
    toto = pd.DataFrame.from_dict(changes)
    toto.set_index('ChangeClass', inplace = True)
    toto['magnitude_diff'] = (toto[toto.columns[[0, 2, 4]]].pow(axis=1, other=2).sum(axis=1))**0.5
    for i in toto.columns[[0, 2, 4]]:
        toto['cos_'+i] = toto[i]/toto['magnitude_diff']
    toto['magnitude_mean'] = (toto[toto.columns[[1, 3, 5]]].pow(axis=1, other=2).sum(axis=1))**0.5
    for i in toto.columns[[1, 3, 5]]:
        toto['cos_'+i] = toto[i]/toto['magnitude_mean']
    return df, toto


def classif_CVA(cva_image_path: str, 
                cva_changelib_path: str = None
                ):
    """
    Function to class CVA image
    Usage:
    """    
    
    start = time.time()
    
    out_path_class = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_classification_clurn.tif')
    out_path_class_jp2 = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_classification_clurn.jp2')
    out_path_dist = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_distance_clurn.tif')
    out_path_dist_jp2 = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_distance_clurn.jp2')  
    
    #~ out_path_class = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_classification_ssm.tif')
    #~ out_path_class_jp2 = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_classification_ssm.jp2')
    #~ out_path_dist = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_distance_ssm.tif')
    #~ out_path_dist_jp2 = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_distance_ssm.jp2')  
    
    #~ out_path_class = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_classification_standardisee_ssm.tif')
    #~ out_path_class_jp2 = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_classification_standardisee_ssm.jp2')
    #~ out_path_dist = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_distance_standardisee_ssm.tif')
    #~ out_path_dist_jp2 = pathlib.Path(cva_image_path).parent / (pathlib.Path(cva_image_path).stem + '_distance_standardisee_ssm.jp2')
    


    if cva_changelib_path is None:
        cva_changelib_path = "/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_01_CVA.csv"
    df = pd.read_csv(cva_changelib_path)
    df = df.set_index('ChangeClass')
    
    with rasterio.open(cva_image_path) as cva_src:
        
        #~ logger.info('Loading magnitude...')
        #~ magnitude = cva_src.read(1).astype(np.float32)
        logger.info('Loading cos x...')
        cosx = cva_src.read(2).astype(np.float32)
        logger.info('Loading cos y...')
        cosy = cva_src.read(3).astype(np.float32)
        logger.info('Loading cos z...')
        cosz = cva_src.read(4).astype(np.float32)
        logger.info('Loading mediane magnitude...')
        magnitude_mean = cva_src.read(8).astype(np.float32)
        logger.info('Loading mediane cos x...')
        cosx_mean = cva_src.read(9).astype(np.float32)
        logger.info('Loading mediane cos y...')
        cosy_mean = cva_src.read(10).astype(np.float32)
        logger.info('Loading mediane cos z...')
        cosz_mean = cva_src.read(11).astype(np.float32)
        
        # Standardisation
        #~ logger.info("Standardisation de l'image...")
        #~ magnitude_m = np.mean(magnitude)
        #~ magnitude_std = np.std(magnitude)
        #~ magnitude_standard = (magnitude - magnitude_m) / magnitude_std
        #~ cosx_m = np.mean(cosx)
        #~ cosx_std = np.std(cosx)
        #~ cosx_standard = (cosx - cosx_m) / cosx_std
        #~ cosy_m = np.mean(cosy)
        #~ cosy_std = np.std(cosy)
        #~ cosy_standard = (cosy - cosy_m) / cosy_std
        #~ cosz_m = np.mean(cosz)
        #~ cosz_std = np.std(cosz)
        #~ cosz_standard = (cosz - cosz_m) / cosz_std
        #~ magnitude_mean_m = np.mean(magnitude_mean)
        #~ magnitude_mean_std = np.std(magnitude_mean)
        #~ magnitude_mean_standard = (magnitude_mean - magnitude_mean_m) / magnitude_mean_std
        #~ cosx_mean_m = np.mean(cosx_mean)
        #~ cosx_mean_std = np.std(cosx_mean)
        #~ cosx_mean_standard = (cosx_mean - cosx_mean_m) / cosx_mean_std
        #~ cosy_mean_m = np.mean(cosy_mean)
        #~ cosy_mean_std = np.std(cosy_mean)
        #~ cosy_mean_standard = (cosy_mean - cosy_mean_m) / cosy_mean_std
        #~ cosz_mean_m = np.mean(cosz_mean)
        #~ cosz_mean_std = np.std(cosz_mean)
        #~ cosz_mean_standard = (cosz_mean - cosz_mean_m) / cosz_mean_std
        
        logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

        magn_profile = cva_src.profile
        
        classification = np.ndarray(shape=(magn_profile['width'],magn_profile['height']), dtype=np.int16)
        distance = np.ndarray(shape=(magn_profile['width'],magn_profile['height']), dtype=np.float32) + 99999
                
        
        for i, cclass in enumerate(df.index):
            logger.info('{} - {}'.format(i+1, cclass))
            
            d = ((cosx - 10000 * df.loc[cclass]['cos_NDVI_diff'])**2 + 
                 (cosy - 10000 * df.loc[cclass]['cos_BIGR_diff'])**2 + 
                 (cosz - 10000 * df.loc[cclass]['cos_NDWIGAO_diff'])**2 + 
                 (cosx_mean - 10000 * df.loc[cclass]['cos_NDVI_mean'])**2 +
                 (cosy_mean - 10000 * df.loc[cclass]['cos_BIGR_mean'])**2 + 
                 (cosz_mean - 10000 * df.loc[cclass]['cos_NDWIGAO_mean'])**2
                 + (magnitude_mean - df.loc[cclass]['magnitude_mean'])**2 \
                 #~ + (magnitude - df.loc[cclass]['magnitude_diff'])**2 \
                 )**0.5
            
            #~ d = ((cosx_standard - (10000 * df.loc[cclass]['cos_NDVI_diff'] - cosx_m) / cosx_std)**2 + 
                 #~ (cosy_standard - (10000 * df.loc[cclass]['cos_BIGR_diff'] - cosy_m) / cosy_std)**2 + 
                 #~ (cosz_standard - (10000 * df.loc[cclass]['cos_NDWIGAO_diff'] - cosz_m) / cosz_std)**2 + 
                 #~ (cosx_mean_standard - (10000 * df.loc[cclass]['cos_NDVI_mean'] - cosx_mean_m) / cosx_mean_std)**2 +
                 #~ (cosy_mean_standard - (10000 * df.loc[cclass]['cos_BIGR_mean'] - cosy_mean_m) / cosy_mean_std)**2 + 
                 #~ (cosz_mean_standard - (10000 * df.loc[cclass]['cos_NDWIGAO_mean'] - cosz_mean_m) / cosz_mean_std)**2
                 #~ + (magnitude_mean_standard - (df.loc[cclass]['magnitude_mean'] - magnitude_mean_m) / magnitude_mean_std)**2 \
                 #~ + (magnitude_standard - (df.loc[cclass]['magnitude_diff'] - magnitude_m) / magnitude_std)**2 \
                 #~ )**0.5
                 
            classification = np.where(d <= distance, i+1, classification)
            distance = np.minimum(distance, d)
                 
                
        # Tests d'optimisation non concluants
        #~ start = time.time()
        #~ logger.info("Creation des matrices 3d")
        #~ cosx_3d = np.repeat(cosx[:,:,np.newaxis],len(df.index), axis=2)
        #~ cosy_3d = np.repeat(cosy[:,:,np.newaxis],len(df.index), axis=2)
        #~ cosz_3d = np.repeat(cosz[:,:,np.newaxis],len(df.index), axis=2)
        #~ cosx_mean_3d = np.repeat(cosx_mean[:,:,np.newaxis],len(df.index), axis=2)
        #~ cosy_mean_3d = np.repeat(cosy_mean[:,:,np.newaxis],len(df.index), axis=2)
        #~ cosz_mean_3d = np.repeat(cosz_mean[:,:,np.newaxis],len(df.index), axis=2)
        #~ magnitude_mean_3d = np.repeat(magnitude_mean[:,:,np.newaxis],len(df.index), axis=2)
        #~ logger.info("Calcul de la matrice des distances")
        #~ d = ((cosx_3d - 10000 * df['cos_NDVI_diff'])**2 +
             #~ (cosy_3d - 10000 * df['cos_BIGR_diff'])**2 + 
             #~ (cosz_3d - 10000 * df['cos_NDWIGAO_diff'])**2 + 
             #~ (cosx_mean_3d - 10000 * df['cos_NDVI_mean'])**2 +
             #~ (cosy_mean_3d - 10000 * df['cos_BIGR_mean'])**2 + 
             #~ (cosz_mean_3d - 10000 * df['cos_NDWIGAO_mean'])**2
             #~ + (magnitude_mean_3d - df['magnitude_mean'])**2 \
             #~ )**0.5
        #~ logger.info("Determination de la classe et distance min")
        #~ classification = np.argmin(d, axis=3)
        #~ distance = np.amin(d, axis=3)
        #~ end = time.time()
        #~ logger.info("time: {}".format(timedelta(seconds = end - start)))
 
    logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

    logger.info("saving TIFF files...")
    magn_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.int16,
                       transform=cva_src.transform,
                       count=1)
    magn_profile.pop('tiled', None)
    magn_profile.pop('nodata', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_class), "w", **magn_profile) as dst:
            dst.write(classification.astype(np.int16), 1)
            
    magn_profile.update(dtype=np.int16)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_dist), "w", **magn_profile) as dst:
            dst.write(distance.astype(np.int16), 1)
    
    logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

    logger.info("converting TIFF files to JPEG2000")
    src_ds = gdal.Open(str(out_path_class))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(str(out_path_class_jp2), src_ds,
                               options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
    dst_ds = None
    src_ds = None
    
    src_ds = gdal.Open(str(out_path_dist))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(str(out_path_dist_jp2), src_ds,
                               options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
    dst_ds = None
    src_ds = None
    os.remove(str(out_path_dist))
    os.remove(str(out_path_class))
    
    legend_classif(class_image_path = str(out_path_class_jp2), \
                   cva_changelib_path = cva_changelib_path)
                   
    end = time.time()
    logger.info("time: {}".format(timedelta(seconds = end - start)))


def legend_classif(class_image_path: str, 
                   input_qml: str = None,
                   cva_changelib_path: str = None
                   ):
                       
    if input_qml is None:
        #~ input_qml = '/DATA_S2/S2_CVA_SEN2CHANGE/Parameters/test_CVA3D_500classes_black.qml'
        input_qml = SHARED_DATA.get("qml_500classes")
    
    if cva_changelib_path is None:
        cva_changelib_path = "/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_01_CVA.csv"
    df = pd.read_csv(cva_changelib_path)
    df = df.set_index('ChangeClass')
        
    out_path_class_qml = pathlib.Path(class_image_path).parent / (pathlib.Path(class_image_path).stem + '.qml')
    
    f = open(str(input_qml), "r")
    txt = f.read()
    f.close()
    
    for i, cclass in enumerate(df.index):
        if any(x in cclass.split('->')[0] for x in ['Cloud_1', 'Cloud_2', 'Shadow']) or any(x in cclass.split('->')[1] for x in ['Cloud_1', 'Cloud_2', 'Shadow']):
            color = "#ff00ff" # MAGENTA
        elif 'Water' not in cclass.split('->')[0] and 'Water' in cclass.split('->')[1]:
            if any(x in cclass.split('->')[0] for x in ['SN_', 'Sand']):
                color = "#66b1cc" # BLEU PALE
            elif any(x in cclass.split('->')[0] for x in ['Veg_1', 'Veg_2']):
                color = "#2c73a9" # BLEU SOMBRE
            else:
                color = "#1a4bff" # BLEU
        elif (not(any(x in cclass.split('->')[0] for x in ['SN_', 'Sand'])) and 'SN_' in cclass.split('->')[1]) or \
             (not(any(x in cclass.split('->')[0] for x in ['SN_', 'Sand'])) and 'Sand' in cclass.split('->')[1]):
            if any(x in cclass.split('->')[0] for x in ['Veg_1', 'Veg_2']):
                color = "#bc8f1e" # ORANGE SOMBRE
            elif 'Water' in cclass.split('->')[0]:
                color = "#f1d029"
            else:
                color = "#e9b226" # ORANGE
        elif 'Veg_2' in cclass.split('->')[1] and  'Veg_1' not in cclass.split('->')[0]:
            color = "#289031" # VERT FONCE
        elif 'Veg_1' in cclass.split('->')[1] and  'Veg_2' not in cclass.split('->')[0]:
            color = "#3dd84a" # VERT CLAIR
        elif 'Veg_2' in cclass.split('->')[1] and  'Veg_1' in cclass.split('->')[0]:
            color = "#3dd84a" # VERT CLAIR
        elif 'Veg_1' in cclass.split('->')[1] and  'Veg_2' in cclass.split('->')[0]:
            color = "#e96c89" # ROUGE CLAIR
        elif 'Veg_1' in cclass.split('->')[0] and  'Veg_2' not in cclass.split('->')[1]:
            color = "#e96c89" # ROUGE CLAIR
        elif 'Veg_2' in cclass.split('->')[0] and  'Veg_1' not in cclass.split('->')[1]:
            color = "#e90d3d" # ROUGE FONCE
        elif any(x in cclass.split('->')[0] for x in ['SN_', 'Sand']) and any(x in cclass.split('->')[0] for x in ['SN_', 'Sand']):
            color = "#adadad" # GRIS CLAIR
        elif ('Water' in cclass.split('->')[0]) and ('Water' in cclass.split('->')[1]):
            color = "#494949" # GRIS FONCE
        else:
            color = "#000000"   # NOIR

        txt = txt.replace('<item value="' + str(i+1) + '" label="' + str(i+1) + '" color="#000000" alpha="255"/>', \
                    '<item value="' + str(i+1) + '" label="' + str(i+1) + " - " + cclass + '" color="' + color + '" alpha="255"/>')
    f = open(str(out_path_class_qml), "w")
    f.write(txt)
    f.close()
    

def reclass_classif(class_image_path: str):
    input_qml = SHARED_DATA.get("qml_9classes")
    #~ input_qml = '/DATA_S2/S2_CVA_SEN2CHANGE/Parameters/9_classes.qml'
    out_path_reclass_tif = pathlib.Path(class_image_path).parent / (pathlib.Path(class_image_path).stem + '_reclass.tif')
    out_path_reclass_jp2 = pathlib.Path(class_image_path).parent / (pathlib.Path(class_image_path).stem + '_reclass.jp2')
    out_path_reclass_qml = pathlib.Path(class_image_path).parent / (pathlib.Path(class_image_path).stem + '_reclass.qml')
    
    with rasterio.open(class_image_path) as class_src:
        class_profile = class_src.profile
        
        logger.info('Loading classification...')
        classification = class_src.read(1).astype(np.uint8)
        
        
        logger.info('Starting reclassification...')
        reclassification = np.zeros_like(classification, dtype=np.int8)
        
        logger.info('#1 SN->SN')
        mat = np.isin(classification, [1,2,3,4,13,14,15,16,25,26,27,28,37,38,39,40,49,50,51,52])
        reclassification = reclassification + 1*mat
        
        logger.info("#2 LowVegIncr")
        mat = np.isin(classification, [5,17,29,41,53,66,90,102,114,126,138,150])
        reclassification = reclassification + 2*mat
        
        logger.info("#3 HighVegIncr")
        mat = np.isin(classification, [6,18,30,42,54,91,103,115,127,139,151])
        reclassification = reclassification + 3*mat
        
        logger.info("#4 SN->Water")
        mat = np.isin(classification, [7,8,9,10,11,12,19,20,21,22,23,24,31,32,33,34,35,36,43,44,45,46,47,48,55,56,57,58,59,60])
        reclassification = reclassification + 4*mat
        
        logger.info("#5 Veg->SN")
        mat = np.isin(classification, [61,62,63,64,65,73,74,75,76,77])
        reclassification = reclassification + 5*mat
        
        logger.info("#6 Veg->Water")
        mat = np.isin(classification, [67,68,69,70,71,72,79,80,81,82,83,84])
        reclassification = reclassification + 6*mat        
        
        logger.info("#7 VegDecr")
        mat = np.isin(classification, [78])
        reclassification = reclassification + 7*mat
        
        logger.info("#8 Water->SN")
        mat = np.isin(classification, [85,86,87,88,89,97,98,99,100,101,109,110,111,112,113,121,122,123,124,125,133,134,135,136,137,145,146,147,148,149])
        reclassification = reclassification + 8*mat
        
        logger.info("#9 Water->Water")
        mat = np.isin(classification, [92,93,94,95,96,104,105,106,107,108,116,117,118,119,120,128,129,130,131,132,140,141,142,143,144,152,153,154,155,156])
        reclassification = reclassification + 9*mat
    
    class_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.uint8,
                       transform=class_src.transform,
                       count=1)
    class_profile.pop('tiled', None)
    class_profile.pop('nodata', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_reclass_tif), "w", **class_profile) as dst:
            dst.write(reclassification.astype(np.uint8), 1)
            
    logger.info("converting TIFF file to JPEG2000")
    src_ds = gdal.Open(str(out_path_reclass_tif))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    #~ for i in range(src_ds.RasterCount):
            #~ src_ds.GetRasterBand(i+1).SetNoDataValue(float(16383))
    
    dst_ds = driver.CreateCopy(str(out_path_reclass_jp2), src_ds,
                               options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])  
    os.remove(str(out_path_reclass_tif))
    
    shutil.copy2(input_qml, str(out_path_reclass_qml))

def computeReclassif(cva_prod = None, nosnw = False):
    input_qml = SHARED_DATA.get("qml_11classes")
    
    out_folder = cva_prod.path
    out_path_tif = cva_prod.classif_SM_CM.parent / (cva_prod.classif_SM_CM.stem + "_RC.tif")
    out_path_jp2 = cva_prod.classif_SM_CM.parent / (cva_prod.classif_SM_CM.stem + "_RC.jp2")
    out_path_qml = cva_prod.classif_SM_CM.parent / (cva_prod.classif_SM_CM.stem + "_RC.qml")
    
    with rasterio.open(cva_prod.classif_SM_CM) as class_src:
        class_profile = class_src.profile
        
        logger.info('Loading classification...')
        classification = class_src.read(1).astype(np.int16, copy=False)
        
        logger.info('Starting reclassification...')
        reclassification = np.zeros_like(classification, dtype=np.int16)
        
        logger.info('#-1 Clouds')
        mat = np.isin(classification, [-1])
        reclassification = reclassification - 1*mat
        
        logger.info('#0 No Change')
        
        if not nosnw:
            logger.info('#1 SN->SN')
            mat = np.isin(classification, [1,2,3,4,13,14,15,16,25,26,27,28,37,38,39,40,49,50,51,52])
            reclassification = reclassification + 1*mat
        else:
            logger.info('#1 SN->SN => #0 No change')
        
        logger.info("#2 LowVegIncr")
        mat = np.isin(classification, [5,17,29,41,53,66,90,102,114,126,138,150])
        reclassification = reclassification + 2*mat
        
        logger.info("#3 HighVegIncr")
        mat = np.isin(classification, [6,18,30,42,54,91,103,115,127,139,151])
        reclassification = reclassification + 3*mat
        
        logger.info("#4 SN->Water")
        mat = np.isin(classification, [7,8,9,10,11,12,19,20,21,22,23,24,31,32,33,34,35,36,43,44,45,46,47,48,55,56,57,58,59,60])
        reclassification = reclassification + 4*mat
        
        logger.info("#5 Veg->SN")
        mat = np.isin(classification, [61,62,63,64,65,73,74,75,76,77])
        reclassification = reclassification + 5*mat
        
        logger.info("#6 Veg->Water")
        mat = np.isin(classification, [67,68,69,70,71,72,79,80,81,82,83,84])
        reclassification = reclassification + 6*mat        
        
        logger.info("#7 VegDecr")
        mat = np.isin(classification, [78])
        reclassification = reclassification + 7*mat
        
        logger.info("#8 Water->SN")
        mat = np.isin(classification, [85,86,87,88,89,97,98,99,100,101,109,110,111,112,113,121,122,123,124,125,133,134,135,136,137,145,146,147,148,149])
        reclassification = reclassification + 8*mat
        
        if not nosnw:
            logger.info("#9 Water->Water")
            mat = np.isin(classification, [92,93,94,95,96,104,105,106,107,108,116,117,118,119,120,128,129,130,131,132,140,141,142,143,144,152,153,154,155,156])
            reclassification = reclassification + 9*mat
        else:
            logger.info('#9 Water->Water => #0 No change')
            
    class_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.int16,
                       transform=class_src.transform,
                       count=1)
    class_profile.pop('tiled', None)
    class_profile.pop('nodata', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_tif), "w", **class_profile) as dst:
            dst.write(reclassification.astype(np.int16), 1)
    
    #~ with rasterio.open(out_path_tif) as class_src:
        #~ class_profile = class_src.profile
        #~ logger.info('Reloading reclassification...')
        #~ logger.info(class_profile)
        #~ classification = class_src.read(1).astype(np.int8, copy=False)
    #~ logger.info('Min:{} / Max: {}'.format(np.min(classification), np.max(classification)))
    
            
    logger.info("converting TIFF file to JPEG2000")
    src_ds = gdal.Open(str(out_path_tif))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    #~ for i in range(src_ds.RasterCount):
            #~ src_ds.GetRasterBand(i+1).SetNoDataValue(float(16383))
    
    dst_ds = driver.CreateCopy(str(out_path_jp2), src_ds,
                               options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])  
    os.remove(str(out_path_tif))
    
    shutil.copy2(str(input_qml), str(out_path_qml))

def post_classif_wet(class_image_path: str, 
                 cva_image_path: str):
    
    out_path = pathlib.Path(class_image_path).parent / (pathlib.Path(class_image_path).stem + '_wet.tif')
    out_path_jp2 = pathlib.Path(class_image_path).parent / (pathlib.Path(class_image_path).stem + '_wet.jp2')
    
    with rasterio.open(class_image_path) as class_src, \
                rasterio.open(cva_image_path) as cva_src:
                    
        class_profile = class_src.profile
        classification = class_src.read(1).astype(np.int8)
        magnitude = cva_src.read(1).astype(np.float32)
        
        reclassification = np.where(magnitude >= 3000, \
                                    np.isin(classification, list(range(7,13)) + 
                                                      list(range(19,25)) +
                                                      list(range(31,37)) +  
                                                      list(range(43,49)) +  
                                                      list(range(55,61)) + 
                                                      list(range(67,73)) +
                                                      list(range(79,85))), \
                                                      0)
        
    class_profile.update(driver = "Gtiff",
                        compress = "NONE",
                        tiled = False,
                        dtype = np.int8,
                        nodata = 0,
                        transform = class_src.transform,
                        count=1)
                        
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **class_profile) as dst:
            dst.write(reclassification.astype(np.int8), 1)
    
    logger.info("converting TIFF file to JPEG2000")
    src_ds = gdal.Open(str(out_path))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    #~ for i in range(src_ds.RasterCount):
            #~ src_ds.GetRasterBand(i+1).SetNoDataValue(float(16383))
    
    dst_ds = driver.CreateCopy(str(out_path_jp2), src_ds,
                               options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
    
    dst_ds = None
    src_ds = None
    os.remove(str(out_path))
    
        
        
        
    






    
