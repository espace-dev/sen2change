# -*- coding: utf-8 -*-

# Copyright (C) 2019  Impact <pascal.mouquet@ird.fr>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module lists all externally useful classes and functions.
"""

from .cvaconfig import Config
#~ from .catlibrary import Catlibrary
#~ from .managelib import *
from .cvaprocess import *
from .cvaclassif import *
from .cvaclassif_multiproc import *
#~ from .cvagestion import *
from .cvaproduct import *
from .cvalibrary import *
#from .temporal_summaries import MonthlySummary, TiledPeriodSummary


__version__ = "v1.00.00"
__author__ = "Impact <pascal.mouquet@ird.fr> & Cyprien <cyprien.alexandre@ird.fr>"
