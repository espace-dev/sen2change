#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for managing CVA products
"""

from pathlib import Path
from typing import List, Dict
import logging
import numpy as np
import rasterio
import pandas as pd
from rasterio.enums import Resampling
from osgeo import gdal
import os, shutil
from datetime import datetime, timedelta
import time
import multiprocessing
import math
import ctypes
from functools import partial


from sen2chain import Tile, L2aProduct, NewCloudMaskProduct

from .cvaconfig import Config, SHARED_DATA
from .cvaprocess import computeCVA
from .cvaclassif_multiproc import computeCVAClassif_multiproc
from .cvaclassif import computeReclassif
from .cvalibrary import CVALibrary

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

global shared_array_waterextent_months_count
shared_array_waterextent_months_count = None
    
global shared_array_nocloud_months_count
shared_array_nocloud_months_count = None
    
global lock
lock = multiprocessing.Lock()

def we_multi(self, months_count, cva_element, def_param=(lock, shared_array_waterextent_months_count, shared_array_nocloud_months_count)):   
    global lock
    
    water_class_list = [7,8,9,10,11,12,19,20,21,22,23,24,31,32,33,34,35,36,43,44,45,46,47,48,55,56,57,58,59,60,
                        67,68,69,70,71,72,79,80,81,82,83,84,
                        92,93,94,95,96,104,105,106,107,108,116,117,118,119,120,128,129,130,131,132,140,141,142,143,144,152,153,154,155,156]
                        
    global shared_array_waterextent_months_count
    global shared_array_nocloud_months_count
    try:
        product = CVAProduct(cva_element)
        if product.l2a_id_bef == self.l2a_id_bef:
            if product.classif_SM_CM:
                with rasterio.open(str(product.classif_SM_CM)) as prod_classif_src:
                    prod_classif_profile = prod_classif_src.profile
                    prod_classif = prod_classif_src.read(1).astype(np.int16)
                lock.acquire() 
                logger.info('Reference Water Extent Multiproc - {} - {}'.format(sum(months_count)+1, cva_element))
                shared_array_waterextent_months_count[:,:,product.daft.month-1] += np.isin(prod_classif, water_class_list).astype(np.int16)
                shared_array_nocloud_months_count[:,:,product.daft.month-1] += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                months_count[product.daft.month-1] += 1
                lock.release()
                
    except:
        pass

class CVAProduct:
    """CVA product class.

    :param identifier: L2A-L2A product identifiers
    #~ :param tile: product's tile.
    #~ :param path: parent folder.
    """
    _cva_root_path = Path(Config().get("cva_path"))
    _date_format = "%Y%m%dT%H%M%S"
    
    def __init__(self,
                 cva_id: str = None,
                 date_cyclone: str = None,
                 ) -> None:
                     
        self.cva_id = cva_id
        self.tile = self.cva_id.split("_")[5][1:]
        self.path = None
        self.l2a_id_bef = self.cva_id.split("-")[0]
        self.dbef = datetime.strptime(self.l2a_id_bef.split("_")[2], self._date_format)
        self.l2a_id_aft = self.cva_id.split("-")[1]
        self.daft = datetime.strptime(self.l2a_id_aft.split("_")[2], self._date_format)
        self.date_cyclone = (datetime.strptime(date_cyclone, "%Y%m%d") if date_cyclone else None)
        self.date_befaf = (self.date_cyclone if self.date_cyclone else self.dbef)
        self._path = self._cva_root_path / self.tile / self.l2a_id_bef / self.l2a_id_aft
        self._pattern_cva = "*_cva.tif"
        self._pattern_magnitude = "*_magnitude.jp2"
        self._pattern_classif = "*_classif.jp2"
        self._pattern_distance = "*_distance.jp2"
        self._pattern_classif_SM = "*_classif_SM.jp2"
        self._pattern_classif_SM_CM = "*_classif_SM_CM.jp2"
        self._pattern_reclassif = "*_classif_SM_CM_RC.jp2"
        self._pattern_temporalcontext = "*_classif_SM_CM_RC_TC-sameclass.jp2"
        
        if self._path.exists():
            self.exists = True
            self.path = self._path
            self.cva = next(self.path.glob(self._pattern_cva), None)
            self.magnitude = next(self.path.glob(self._pattern_magnitude), None)
            self.classif = next(self.path.glob(self._pattern_classif), None)
            self.distance = next(self.path.glob(self._pattern_distance), None)
            self.classif_SM = next(self.path.glob(self._pattern_classif_SM), None)
            self.classif_SM_CM = next(self.path.glob(self._pattern_classif_SM_CM), None)
            self.reclassif = next(self.path.glob(self._pattern_reclassif), None)
            self.temporalcontext = next(self.path.glob(self._pattern_temporalcontext), None)
            
            
        else:
            self.exists = False
            self.path, self.cva, self.magnitude, self.classif, self.distance, self.classif_SM, self.classif_SM_CM, self.reclassif, self.temporalcontext = [None]*9
        
        #~ self.safe = self.identifier + ".SAFE"
                     
        #~ super().__init__(identifier=identifier, tile=tile, path=path)
    
    def computeAll(self,
                   reprocess: bool = False,
                   removecva:bool = True):
        self.initialize()
        self.computeCVA(reprocess)
        self.computeClassif(reprocess, removecva)
        self.computeSeuillageMagnitude(reprocess)
        self.computeCloudMaskClassif(reprocess)
        self.computeReclassif(reprocess)
        self.computeTemporalContext(reprocess)    
    
    def initialize(self, cm_version: str = "CM004"):
        logger.info("Initialization")
        #~ _path = self._cva_root_path / self.tile / self.cva_id
        self._path.mkdir(parents = True, exist_ok = True)
        self.exists = True
        self.path = self._path
        #~ L2aProduct(self.l2a_id_bef).process_cloud_mask_v2()
        L2aProduct(self.l2a_id_bef).compute_cloud_mask(cm_version = cm_version)
        #~ L2aProduct(self.l2a_id_bef).process_indices(["NDVI", "BIGR", "NDWIGAO"], True, True)
        L2aProduct(self.l2a_id_bef).compute_indice("NDVI", cm_version = cm_version)
        L2aProduct(self.l2a_id_bef).compute_indice("BIGR", cm_version = cm_version)
        L2aProduct(self.l2a_id_bef).compute_indice("NDWIGAO", cm_version = cm_version)
        #~ L2aProduct(self.l2a_id_aft).process_cloud_mask_v2()
        L2aProduct(self.l2a_id_aft).compute_cloud_mask(cm_version = cm_version)
        #~ L2aProduct(self.l2a_id_aft).process_indices(["NDVI", "BIGR", "NDWIGAO"], True, True)
        L2aProduct(self.l2a_id_aft).compute_indice("NDVI", cm_version = cm_version)
        L2aProduct(self.l2a_id_aft).compute_indice("BIGR", cm_version = cm_version)
        L2aProduct(self.l2a_id_aft).compute_indice("NDWIGAO", cm_version = cm_version)
    
    def computeCVA(self,
                   reprocess: bool = False):
        if (not self.cva) or (self.cva and reprocess):
            logger.info("Computing CVA")
            computeCVA(self)
            self.cva = next(self.path.glob(self._pattern_cva), None)
            self.magnitude = next(self.path.glob(self._pattern_magnitude), None)

        else:
            logger.info("Nothing to do - CVA already computed")
    
    def computeClassif(self,
                       reprocess: bool = False,
                       removecva: bool = False):
        if (not self.classif) or (self.classif and reprocess):
            logger.info("Computing CVA Classif")
            if self.cva:
                computeCVAClassif_multiproc(self)
                self.classif = next(self.path.glob(self._pattern_classif), None)
                self.distance = next(self.path.glob(self._pattern_distance), None)
            else:
                logger.info("CVA not found, compute CVA first")
        else:
            logger.info("Nothing to do - CVA Classif already computed")
        if removecva:
            logger.info("Removing CVA")
            self.cva.unlink()
            self.cva = next(self.path.glob(self._pattern_cva), None)
        
    def computeSeuillageMagnitude(self,
                                  reprocess: bool = False,
                                  mode = 1,
                                  seuil = 3000):
        if (not self.classif_SM) or (self.classif_SM and reprocess):
            logger.info("Computing Seuillage de la Magnitude")
            out_path_tif = self.classif.parent / (self.classif.stem + "_SM.tif")
            out_path_jp2 = self.classif.parent / (self.classif.stem + "_SM.jp2")

            with rasterio.open(str(self.classif)) as classif_src, \
                 rasterio.open(str(self.magnitude)) as magnitude_src:
                    
                classif_profile = classif_src.profile
                classif = classif_src.read(1).astype(np.int16)
                magnitude = magnitude_src.read(1).astype(np.float32)
            
            if mode == 1:
                # 1 seuil de magnitude fixe    
                csm = np.where(magnitude >= seuil, classif, 0)
                
            elif mode ==2:
                # 2 seuil variable en fonction de la classe
                #~ cva_changelib_path = "/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_01_CVA.csv"
                #~ cva_changelib_path = '/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_02_01_changelib1std.csv'
                #~ cva_changelib_path = '/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_02_02_changelib2std.csv'
                # cva_changelib_path = '/DATA_S2/TEMP/PM/TIME_SERIES/20190521_test_occsol/Classes_changement_02_02_03_changelib3std.csv'
                cva_changelib_path = Path(Config().get("cva_changelib_path "))
    
                df = pd.read_csv(cva_changelib_path)
                df = df.set_index('ChangeClass')
    
                    #2.1 moyenne (+-ET)
                    #2.2 mediane (+-ET)
                    #2.3 etc.
                #~ seuils = np.where(classif > 0, df.magnitude_diff.values[classif-1].astype(np.int16), 0)
                seuils = np.where(classif > 0, df.min_mg.values[classif-1].astype(np.int16), 0)
                csm = np.where(magnitude >= seuils, classif, 0)
            
            classif_profile.update(driver="Gtiff",
                                   compress="NONE",
                                   tiled=False,
                                   dtype=np.int16,
                                   nodata=0,
                                   transform=classif_src.transform,
                                   count=1)
            classif_profile.pop('tiled', None)
            classif_profile.pop('nodata', None)
                
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(out_path_tif), "w", **classif_profile) as dst:
                    dst.write(csm, 1)
                        
            src_ds = gdal.Open(str(out_path_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(out_path_jp2), src_ds,
                                   options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(out_path_tif))
            
            self.classif_SM = next(self.path.glob(self._pattern_classif_SM), None)
            shutil.copy2(str(self.classif.parent / (self.classif.stem + ".qml")), \
                         str(self.classif_SM.parent / (self.classif_SM.stem + '.qml')))
        else:
            logger.info("Nothing to do - CVA Classif Seuillage Magnitude already computed")
    
    def computeCloudMaskClassif(self,
                                reprocess: bool = False,
                                cm_version: str = "CM004"):
        if (not self.classif_SM_CM) or (self.classif_SM_CM and reprocess):
            logger.info("Masking CVA classif Seuil Magnitude with Cloud Mask {} (and AOT for nodata)...".format(cm_version))
            #~ cldmsk_bef_path = Tile(self.tile)._paths['l2a'] / (self.l2a_id_bef + "_CLOUD_MASK_B11.jp2")
            #~ cldmsk_aft_path = Tile(self.tile)._paths['l2a'] / (self.l2a_id_aft + "_CLOUD_MASK_B11.jp2")
            cldmsk_bef_path = NewCloudMaskProduct(l2a_identifier = self.l2a_id_bef, cm_version = cm_version).path
            cldmsk_aft_path = NewCloudMaskProduct(l2a_identifier = self.l2a_id_aft, cm_version = cm_version).path
            
            
            aot_bef_path = L2aProduct(self.l2a_id_bef).aot_20m
            aot_aft_path = L2aProduct(self.l2a_id_aft).aot_20m
            
            out_path_tif = self.classif_SM.parent / (self.classif_SM.stem + "_CM.tif")
            out_path_jp2 = self.classif_SM.parent / (self.classif_SM.stem + "_CM.jp2")
            
            with rasterio.open(str(cldmsk_bef_path)) as cldmsk_bef_src, \
                 rasterio.open(str(cldmsk_aft_path)) as cldmsk_aft_src, \
                 rasterio.open(str(aot_bef_path)) as aot_bef_src, \
                 rasterio.open(str(aot_aft_path)) as aot_aft_src, \
                 rasterio.open(str(self.classif_SM)) as csm_src:
                
                csm_profile = csm_src.profile
                csm = csm_src.read(1).astype(np.int16)
                cldmsk_bef = cldmsk_bef_src.read(1, out_shape = csm.shape, resampling=Resampling.nearest).astype(np.int8)
                cldmsk_aft = cldmsk_aft_src.read(1, out_shape = csm.shape, resampling=Resampling.nearest).astype(np.int8)
                aot_bef = aot_bef_src.read(1, out_shape = csm.shape, resampling=Resampling.nearest).astype(np.int16)
                aot_aft = aot_aft_src.read(1, out_shape = csm.shape, resampling=Resampling.nearest).astype(np.int16)
            
            csmcm = np.where((1 - cldmsk_bef) * (1 - cldmsk_aft) * (aot_bef != 0) * (aot_aft != 0), csm, -1)
            
            csm_profile.update(driver="Gtiff",
                                 compress="NONE",
                                 tiled=False,
                                 dtype=np.int16,
                                 nodata=0,
                                 transform=csm_src.transform,
                                 count=1)
            csm_profile.pop('tiled', None)
            csm_profile.pop('nodata', None)
            
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(out_path_tif), "w", **csm_profile) as dst:
                    dst.write(csmcm, 1)
            
                #~ logger.info("converting {} file to JP2000...".format(out_path_tif.name))
    
            src_ds = gdal.Open(str(out_path_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(out_path_jp2), src_ds,
                                   options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO", "YCC=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(out_path_tif))
            
            self.classif_SM_CM = next(self.path.glob(self._pattern_classif_SM_CM), None)
            shutil.copy2(str(self.classif_SM.parent / (self.classif_SM.stem + ".qml")), \
                         str(self.classif_SM_CM.parent / (self.classif_SM_CM.stem + '.qml')))
        else:
            logger.info("Nothing to do - Cloud mask already applied")
        
        
    def computeReclassif(self,
                         reprocess: bool = False):
        if (not self.reclassif) or (self.reclassif and reprocess):
            logger.info("Computing Reclassification")
            computeReclassif(self, nosnw = True)
            self.reclassif = next(self.path.glob(self._pattern_reclassif), None)
        else:
            logger.info("Nothing to do - Reclassification already done")
    
    def computeTemporalContext(self,
                               reprocess: bool = False):
        if (not self.temporalcontext) or (self.temporalcontext and reprocess):
            start = time.time()
            logger.info("temporal context - computing...")
            if self.date_cyclone:
                logger.info("using cyclone date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
            else:
                logger.info("cyclone date not set, using ref date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
            
            outpath_sameclass_tif = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclass.tif")
            outpath_sameclass_jp2 = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclass.jp2")
            
            outpath_sameclassbefore_tif = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclassbefore-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
            outpath_sameclassbefore_jp2 = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclassbefore-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
            
            outpath_sameclassafter_tif = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclassafter-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
            outpath_sameclassafter_jp2 = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclassafter-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
            
            outpath_sameclassdiff_tif = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclasdiff-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
            outpath_sameclassdiff_jp2 = self.reclassif.parent / (self.reclassif.stem + "_TC-sameclassdiff-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
            
            lib = CVALibrary()
            cva_list = getattr(lib, self.tile)
            count = 0
            countbefore = 0
            countafter = 0
            
            with rasterio.open(str(self.reclassif)) as ref_classif_src:
                ref_classif_profile = ref_classif_src.profile
                ref_classif = ref_classif_src.read(1).astype(np.int16)
            
            for identifier in [x for x in cva_list if x not in self.cva_id]:
                try:
                    product = CVAProduct(identifier)
                    if product.l2a_id_bef == self.l2a_id_bef:
                        if product.reclassif:
                            count += 1
                            logger.info("{} - {}".format(count, product.cva_id))
                            
                            with rasterio.open(str(product.reclassif)) as prod_classif_src:
                                prod_classif = prod_classif_src.read(1).astype(np.int16)
                            try:
                                sameclass_count += (ref_classif == prod_classif).astype(np.int16)
                                nocloud_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            except:
                                sameclass_count = (ref_classif == prod_classif).astype(np.int16)
                                nocloud_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            
                            if product.daft < self.date_befaf:
                                countbefore += 1
                                try:
                                    sameclassbefore_count += (ref_classif == prod_classif).astype(np.int16)
                                    nocloudbefore_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                                except:
                                    sameclassbefore_count = (ref_classif == prod_classif).astype(np.int16)
                                    nocloudbefore_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            
                            if product.daft > self.date_befaf:
                                countafter += 1
                                try:
                                    sameclassafter_count += (ref_classif == prod_classif).astype(np.int16)
                                    nocloudafter_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                                except:
                                    sameclassafter_count = (ref_classif == prod_classif).astype(np.int16)
                                    nocloudafter_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                except:
                    pass
            
            logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

            if count:
                logger.info("Processing same class composite image from {} images".format(count))
    
                sameclass_count = ((ref_classif > 0) * sameclass_count - 1 * (ref_classif == -1) +  (ref_classif == 0) * nocloud_count).astype(np.int16) 
                nocloud_count = np.where(ref_classif != -1, nocloud_count, -1).astype(np.int16)
                with np.errstate(divide='ignore', invalid='ignore'):
                    sameclass_pct = np.where(ref_classif != -1, (100*(np.where(nocloud_count > 0, sameclass_count / nocloud_count, 0))), -1).astype(np.int16)
                
                ref_classif_profile.update(driver="Gtiff",
                                           compress="NONE",
                                           tiled=False,
                                           dtype=np.int16,
                                           nodata=0,
                                           transform=ref_classif_src.transform,
                                           count=1)
                ref_classif_profile.pop('tiled', None)
                ref_classif_profile.pop('nodata', None)
                with rasterio.Env(GDAL_CACHEMAX=512) as env:
                    with rasterio.open(str(outpath_sameclass_tif), "w", **ref_classif_profile) as dst:
                        dst.write(sameclass_pct, 1)
                
                logger.info("Converting to JPEG2000")
                src_ds = gdal.Open(str(outpath_sameclass_tif))
                driver = gdal.GetDriverByName("JP2OpenJPEG")
                dst_ds = driver.CreateCopy(str(outpath_sameclass_jp2), src_ds,
                                           options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
                dst_ds = None
                src_ds = None
                os.remove(str(outpath_sameclass_tif))
                self.reclassif = next(self.path.glob(self._pattern_reclassif), None)
            else:
                logger.info("Temporal context - No other matching CVA")
        
            if countbefore:
                logger.info("Temporal context - Processing same class before composite image from {} images".format(countbefore))
    
                sameclassbefore_count = ((ref_classif > 0) * sameclassbefore_count - 1 * (ref_classif == -1) +  (ref_classif == 0) * nocloudbefore_count).astype(np.int16) 
                nocloudbefore_count = np.where(ref_classif != -1, nocloudbefore_count, -1).astype(np.int16)
                with np.errstate(divide='ignore', invalid='ignore'):
                    sameclassbefore_pct = np.where(ref_classif != -1, (100*(np.where(nocloudbefore_count > 0, sameclassbefore_count / nocloudbefore_count, 0))), -1).astype(np.int16)
                
                with rasterio.Env(GDAL_CACHEMAX=512) as env:
                    with rasterio.open(str(outpath_sameclassbefore_tif), "w", **ref_classif_profile) as dst:
                        dst.write(sameclassbefore_pct, 1)
                
                logger.info("Temporal context - Converting to JPEG2000")
                
                src_ds = gdal.Open(str(outpath_sameclassbefore_tif))
                driver = gdal.GetDriverByName("JP2OpenJPEG")
                dst_ds = driver.CreateCopy(str(outpath_sameclassbefore_jp2), src_ds,
                                           options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
                dst_ds = None
                src_ds = None
                os.remove(str(outpath_sameclassbefore_tif))
                
            if countafter:
                logger.info("Temporal context - Processing same class after composite image from {} images".format(countafter))
    
                sameclassafter_count = ((ref_classif > 0) * sameclassafter_count - 1 * (ref_classif == -1) +  (ref_classif == 0) * nocloudafter_count).astype(np.int16) 
                nocloudafter_count = np.where(ref_classif != -1, nocloudafter_count, -1).astype(np.int16)
                with np.errstate(divide='ignore', invalid='ignore'):
                    sameclassafter_pct = np.where(ref_classif != -1, (100*(np.where(nocloudafter_count > 0, sameclassafter_count / nocloudafter_count, 0))), -1).astype(np.int16)
                
                with rasterio.Env(GDAL_CACHEMAX=512) as env:
                    with rasterio.open(str(outpath_sameclassafter_tif), "w", **ref_classif_profile) as dst:
                        dst.write(sameclassafter_pct, 1)
                
                logger.info("Temporal context - Converting to JPEG2000")
                src_ds = gdal.Open(str(outpath_sameclassafter_tif))
                driver = gdal.GetDriverByName("JP2OpenJPEG")
                dst_ds = driver.CreateCopy(str(outpath_sameclassafter_jp2), src_ds,
                                           options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
                dst_ds = None
                src_ds = None
                os.remove(str(outpath_sameclassafter_tif))
            
            if countbefore and countafter:
                logger.info("Temporal context - Processing after-before difference")
                
                sameclassdiff_pct = np.where(ref_classif != -1, sameclassafter_pct - sameclassbefore_pct, -1001).astype(np.int16)
                
                with rasterio.Env(GDAL_CACHEMAX=512) as env:
                    with rasterio.open(str(outpath_sameclassdiff_tif), "w", **ref_classif_profile) as dst:
                        dst.write(sameclassdiff_pct, 1)
                
                logger.info("Temporal context - Converting to JPEG2000")
                src_ds = gdal.Open(str(outpath_sameclassdiff_tif))
                driver = gdal.GetDriverByName("JP2OpenJPEG")
                dst_ds = driver.CreateCopy(str(outpath_sameclassdiff_jp2), src_ds,
                                           options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
                dst_ds = None
                src_ds = None
                os.remove(str(outpath_sameclassdiff_tif))
        else:
            logger.info("Temporal context - Nothing to do - Temporal context already done")
        
    def computeRefNoChange(self):
        if self.date_cyclone:
            logger.info("Reference temporal context - using cyclone date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
        else:
            logger.info("Reference temporal context - cyclone date not set, using ref date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
        
        outpath_folder = self._path.parent / "TEMPORAL_ANALYSIS"
        outpath_folder.mkdir(parents = True, exist_ok = True)
        
        outpath_nochange_tif = outpath_folder / (self.l2a_id_bef + "_nochange.tif")
        outpath_nochange_jp2 = outpath_folder / (self.l2a_id_bef + "_nochange.jp2")
        
        outpath_nochangebefore_tif = outpath_folder / (self.l2a_id_bef + "_nochangebefore-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
        outpath_nochangebefore_jp2 = outpath_folder / (self.l2a_id_bef + "_nochangebefore-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
        
        outpath_nochangeafter_tif = outpath_folder / (self.l2a_id_bef + "_nochangeafter-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
        outpath_nochangeafter_jp2 = outpath_folder / (self.l2a_id_bef + "_nochangeafter-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
        
        outpath_nochangediff_tif = outpath_folder / (self.l2a_id_bef + "_nochangediff-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
        outpath_nochangediff_jp2 = outpath_folder / (self.l2a_id_bef + "_nochangediff-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
        
        
        lib = CVALibrary()
        cva_list = getattr(lib, self.tile)
        count = 0
        countbef = 0
        countaft = 0
        #~ for identifier in cva_list[::10]:
        for identifier in cva_list:
            try:
                product = CVAProduct(identifier)
                if product.l2a_id_bef == self.l2a_id_bef:
                    if product.reclassif:
                        count += 1
                        logger.info("{} - {}".format(count, product.cva_id))
                        with rasterio.open(str(product.reclassif)) as prod_classif_src:
                            prod_classif_profile = prod_classif_src.profile
                            prod_classif = prod_classif_src.read(1).astype(np.int16)
                        try:
                            nochange_total_count += np.where(prod_classif == 0, 1, 0).astype(np.int16)
                            nocloud_total_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                        except:
                            nochange_total_count = np.where(prod_classif == 0, 1, 0).astype(np.int16)
                            nocloud_total_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            
                        if product.daft < self.date_befaf:
                            countbef += 1
                            try:
                                nochange_before_count += np.where(prod_classif == 0, 1, 0).astype(np.int16)
                                nocloud_before_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            except:
                                nochange_before_count = np.where(prod_classif == 0, 1, 0).astype(np.int16)
                                nocloud_before_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                        
                        if product.daft > self.date_befaf:
                            countaft += 1
                            try:
                                nochange_after_count += np.where(prod_classif == 0, 1, 0).astype(np.int16)
                                nocloud_after_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            except:
                                nochange_after_count = np.where(prod_classif == 0, 1, 0).astype(np.int16)
                                nocloud_after_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
            except:
                pass
            
        if count:
            logger.info("Reference temporal context - Processing nochange composite image from {} images".format(count))
            with np.errstate(divide='ignore', invalid='ignore'):
                nochange_total_pct = (np.where(nocloud_total_count > 0, 100*(nochange_total_count / nocloud_total_count), -1)).astype(np.int16)
            
            prod_classif_profile.update(driver="Gtiff",
                                       compress="NONE",
                                       dtype=np.int16,
                                       transform=prod_classif_src.transform,
                                       count=1)
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_nochange_tif), "w", **prod_classif_profile) as dst:
                    dst.write(nochange_total_pct, 1)
            src_ds = gdal.Open(str(outpath_nochange_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_nochange_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_nochange_tif))
        
        if countbef:
            logger.info("Reference temporal context - Processing nochange before composite image from {} images".format(countbef))
            with np.errstate(divide='ignore', invalid='ignore'):
                nochange_before_pct = (np.where(nocloud_before_count > 0, 100*(nochange_before_count / nocloud_before_count), -1)).astype(np.int16)
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_nochangebefore_tif), "w", **prod_classif_profile) as dst:
                    dst.write(nochange_before_pct, 1)
            src_ds = gdal.Open(str(outpath_nochangebefore_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_nochangebefore_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_nochangebefore_tif))
        
        if countaft:
            logger.info("Reference temporal context - Processing nochange after composite image from {} images".format(countaft))
            with np.errstate(divide='ignore', invalid='ignore'):
                nochange_after_pct = (np.where(nocloud_after_count > 0, 100*(nochange_after_count / nocloud_after_count), -1)).astype(np.int16)
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_nochangeafter_tif), "w", **prod_classif_profile) as dst:
                    dst.write(nochange_after_pct, 1)
            src_ds = gdal.Open(str(outpath_nochangeafter_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_nochangeafter_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_nochangeafter_tif))
        
        if countbef and countaft:
            logger.info("Reference temporal context - Processing Nochange after-before difference")
            nochangediff_pct = nochange_after_pct - nochange_before_pct
            
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_nochangediff_tif), "w", **prod_classif_profile) as dst:
                    dst.write(nochangediff_pct, 1)
            
            logger.info("Reference temporal context - Converting to JPEG2000")
            src_ds = gdal.Open(str(outpath_nochangediff_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_nochangediff_jp2), src_ds,
                                           options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_nochangediff_tif))
            
        
        #~ else:
            #~ logger.info("Reference temporal context - Nothing to do - Temporal context already done")
            
    def computeRefClassDiversity(self):
        if self.date_cyclone:
            logger.info("Reference temporal context - using cyclone date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
        else:
            logger.info("Reference temporal context - cyclone date not set, using ref date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
        
        outpath_classdiversity_tif = self.path.parent / (self.l2a_id_bef + "_classdiversity.tif")
        outpath_classdiversity_jp2 = self.path.parent / (self.l2a_id_bef + "_classdiversity.jp2")
        
        outpath_classdiversitybefore_tif = self.path.parent / (self.l2a_id_bef + "_classdiversitybefore-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
        outpath_classdiversitybefore_jp2 = self.path.parent / (self.l2a_id_bef + "_classdiversitybefore-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
        
        outpath_classdiversityafter_tif = self.path.parent / (self.l2a_id_bef + "_classdiversityafter-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
        outpath_classdiversityafter_jp2 = self.path.parent / (self.l2a_id_bef + "_classdiversityafter-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")
        
        outpath_classdiversitydiff_tif = self.path.parent / (self.l2a_id_bef + "_classdiversitydiff-"+ self.date_befaf.strftime("%Y%m%d") +".tif")
        outpath_classdiversitydiff_jp2 = self.path.parent / (self.l2a_id_bef + "_classdiversitydiff-"+ self.date_befaf.strftime("%Y%m%d") +".jp2")        
        
        popcount_table16 = np.zeros(2**16, dtype=int)
        for index in range(len(popcount_table16)):
            popcount_table16[index] = (index & 1) + popcount_table16[index >> 1]
        
        lib = CVALibrary()
        cva_list = getattr(lib, self.tile)
        count = 0
        countbef = 0
        countaft = 0
        #~ for identifier in cva_list[::10]:
        for identifier in cva_list:
            try:
                product = CVAProduct(identifier)
                if product.l2a_id_bef == self.l2a_id_bef:
                    if product.reclassif:
                        count += 1
                        logger.info("{} - {}".format(count, product.cva_id))
                        with rasterio.open(str(product.reclassif)) as prod_classif_src:
                            prod_classif_profile = prod_classif_src.profile
                            prod_classif = prod_classif_src.read(1).astype(np.int16)
                        try:
                            nocloud_total_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            classdiversity_total_count = np.bitwise_or(classdiversity_total_count, 
                                                                       (2**np.where(prod_classif >=0, prod_classif, -math.inf)).astype(np.int16), 
                                                                        classdiversity_total_count)
                        except:
                            nocloud_total_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                            classdiversity_total_count = (2**np.where(prod_classif >=0, prod_classif, -math.inf)).astype(np.int16)
                            
                        if product.daft < self.date_befaf:
                            countbef += 1
                            try:
                                nocloud_before_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                                classdiversity_before_count = np.bitwise_or(classdiversity_before_count, 
                                                                       (2**np.where(prod_classif >=0, prod_classif, -math.inf)).astype(np.int16), 
                                                                        classdiversity_before_count)
                            except:
                                nocloud_before_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                                classdiversity_before_count = (2**np.where(prod_classif >=0, prod_classif, -math.inf)).astype(np.int16)
                        
                        if product.daft > self.date_befaf:
                            countaft += 1
                            try:
                                nocloud_after_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                                classdiversity_after_count = np.bitwise_or(classdiversity_after_count, 
                                                                       (2**np.where(prod_classif >=0, prod_classif, -math.inf)).astype(np.int16), 
                                                                        classdiversity_after_count)
                            except:
                                nocloud_after_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                                classdiversity_after_count = (2**np.where(prod_classif >=0, prod_classif, -math.inf)).astype(np.int16)
            except:
                pass
            
        if count:
            prod_classif_profile.update(driver="Gtiff",
                                       compress="NONE",
                                       dtype=np.int16,
                                       transform=prod_classif_src.transform,
                                       count=1)
            logger.info("Reference temporal context - Processing class diversity from {} images".format(count))
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_classdiversity_tif), "w", **prod_classif_profile) as dst:
                    dst.write(popcount_table16[classdiversity_total_count].astype(np.int16), 1)
            src_ds = gdal.Open(str(outpath_classdiversity_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_classdiversity_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_classdiversity_tif))
                        
        if countbef:
            logger.info("Reference temporal context - Processing class diversity before from {} images".format(countbef))
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_classdiversitybefore_tif), "w", **prod_classif_profile) as dst:
                    dst.write(popcount_table16[classdiversity_before_count].astype(np.int16), 1)
            src_ds = gdal.Open(str(outpath_classdiversitybefore_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_classdiversitybefore_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_classdiversitybefore_tif))
                               
        if countaft:
            logger.info("Reference temporal context - Processing class diversity after from {} images".format(countaft))
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_classdiversityafter_tif), "w", **prod_classif_profile) as dst:
                    dst.write(popcount_table16[classdiversity_after_count].astype(np.int16), 1)
            src_ds = gdal.Open(str(outpath_classdiversityafter_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_classdiversityafter_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_classdiversityafter_tif))
                    
        if countbef and countaft:
            logger.info("Reference temporal context - Processing class diversity after-before difference")
            classdiversitydiff = popcount_table16[classdiversity_after_count] - popcount_table16[classdiversity_before_count]
            
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_classdiversitydiff_tif), "w", **prod_classif_profile) as dst:
                    dst.write(classdiversitydiff.astype(np.int16), 1)
            src_ds = gdal.Open(str(outpath_classdiversitydiff_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_classdiversitydiff_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_classdiversitydiff_tif))
        
        #~ else:
            #~ logger.info("Reference temporal context - Nothing to do - Temporal context already done")
            

    def computeRefWaterExtent(self):

        start = time.time()
        
        outpath_waterextent_tif = self.path.parent / (self.l2a_id_bef + "_waterextent.tif")
        outpath_waterextent_jp2 = self.path.parent / (self.l2a_id_bef + "_waterextent.jp2")
        
        outpath_waterextent_jan_tif = self.path.parent / (self.l2a_id_bef + "_waterextent_jan.tif")
        outpath_waterextent_jan_jp2 = self.path.parent / (self.l2a_id_bef + "_waterextent_jan.jp2")
        
        lib = CVALibrary()
        cva_list = getattr(lib, self.tile)
        count = 0
        countbef = 0
        countaft = 0
        count_jan = 0
        water_class_list = [7,8,9,10,11,12,19,20,21,22,23,24,31,32,33,34,35,36,43,44,45,46,47,48,55,56,57,58,59,60,
                            67,68,69,70,71,72,79,80,81,82,83,84,
                            92,93,94,95,96,104,105,106,107,108,116,117,118,119,120,128,129,130,131,132,140,141,142,143,144,152,153,154,155,156]
        
        waterextent_months_count = np.zeros((10980,10980,12), dtype=np.int16)
        nocloud_months_count = np.zeros((10980,10980,12), dtype=np.int16)
        months = ["01_jan", "02_feb", "03_mar", "04_apr", "05_may", "06_jun", "07_jul", "08_aug", "09_sep", "10_oct", "11_nov", "12_dec"]
        months_count = [0]*12
        outpath_waterextent_months_tif = [a+b+c for a in [str(self.path.parent / (self.l2a_id_bef + "_waterextent_"))] for b in months for c in [".tif"]]
        outpath_waterextent_months_jp2 = [a+b+c for a in [str(self.path.parent / (self.l2a_id_bef + "_waterextent_"))] for b in months for c in [".jp2"]]
        
        for identifier in cva_list:
            try:
                product = CVAProduct(identifier)
                if product.l2a_id_bef == self.l2a_id_bef:
                    if product.classif_SM_CM:
                        count += 1
                        logger.info("Reference water extent - {} - {}".format(count, product.cva_id))
                        with rasterio.open(str(product.classif_SM_CM)) as prod_classif_src:
                            prod_classif_profile = prod_classif_src.profile
                            prod_classif = prod_classif_src.read(1).astype(np.int16)
                        #~ try:
                            #~ waterextent_total_count += np.isin(prod_classif, water_class_list).astype(np.int16)
                            #~ nocloud_total_count += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                        #~ except:
                            #~ waterextent_total_count = np.isin(prod_classif, water_class_list).astype(np.int16)
                            #~ nocloud_total_count = np.where(prod_classif == -1, 0, 1).astype(np.int16)
                        
                        waterextent_months_count[:,:,product.daft.month-1] += np.isin(prod_classif, water_class_list).astype(np.int16)
                        nocloud_months_count[:,:,product.daft.month-1] += np.where(prod_classif == -1, 0, 1).astype(np.int16)
                        months_count[product.daft.month-1] += 1
            except:
                logger.info("Erreur!!")
        
        logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

        prod_classif_profile.update(driver="Gtiff",
                                    compress="NONE",
                                    dtype=np.int16,
                                    transform=prod_classif_src.transform,
                                    count=1)
            
        #~ if count:
            #~ logger.info("Reference water extent - Processing water extent composite image from {} images".format(count))
            #~ with np.errstate(divide='ignore', invalid='ignore'):
                #~ waterextent_total_pct = (np.where(nocloud_total_count > 0, 100*(waterextent_total_count / nocloud_total_count), -1)).astype(np.int16)
            
            #~ with rasterio.Env(GDAL_CACHEMAX=512) as env:
                #~ with rasterio.open(str(outpath_waterextent_tif), "w", **prod_classif_profile) as dst:
                    #~ dst.write(waterextent_total_pct, 1)
                    
        # Water Extent Total
        logger.info("Reference water extent - Processing water extent composite image from {} images".format(count))
        with np.errstate(divide='ignore', invalid='ignore'):
                waterextent_total_pct = (np.where(np.sum(nocloud_months_count, 2) > 0, 
                                                         100*(np.sum(waterextent_months_count, 2) / np.sum(nocloud_months_count, 2)),
                                                         -1)).astype(np.int16)
        with rasterio.Env(GDAL_CACHEMAX=512) as env:
            with rasterio.open(str(outpath_waterextent_tif), "w", **self_classif_profile) as dst:
                dst.write(waterextent_total_pct, 1)
        
        
        for i in range(len(months)):
            logger.info("Reference water extent - Processing {} water extent composite image from {} images".format(months[i], months_count[i]))
            logger.info(waterextent_months_count[:,:,i])
            logger.info(nocloud_months_count[:,:,i])

            with np.errstate(divide='ignore', invalid='ignore'):
                waterextent_month_pct = (np.where(nocloud_months_count[:,:,i] > 0, 100*(waterextent_months_count[:,:,i] / nocloud_months_count[:,:,i]), -1)).astype(np.int16)
            
            logger.info(waterextent_month_pct)
            
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_waterextent_months_tif[i]), "w", **prod_classif_profile) as dst:
                    dst.write(waterextent_month_pct, 1)   
    
    def computeRefWaterExtent_multiproc(self):
        """
        Function to monthly statistics on water extents
        """
        start = time.time()
        
        outpath_waterextent_pct_tif = self.path.parent / (self.l2a_id_bef + "_waterextent_pct.tif")
        #~ outpath_waterextent_pct_jp2 = self.path.parent / (self.l2a_id_bef + "_waterextent_pct.jp2")
        
        outpath_waterextent_cnt_tif = self.path.parent / (self.l2a_id_bef + "_waterextent_cnt.tif")
        #~ outpath_waterextent_cnt_jp2 = self.path.parent / (self.l2a_id_bef + "_waterextent.cntjp2")
        
        
        lib = CVALibrary()
        cva_list = getattr(lib, self.tile)
        count = 0
        
        months = ["01_jan", "02_feb", "03_mar", "04_apr", "05_may", "06_jun", "07_jul", "08_aug", "09_sep", "10_oct", "11_nov", "12_dec"]
        man = multiprocessing.Manager()
        months_count = man.list([0]*12)
        outpath_waterextent_months_pct_tif = [a+b+c for a in [str(self.path.parent / (self.l2a_id_bef + "_waterextent_"))] \
                                                for b in months \
                                                for c in ["_pct.tif"]]
        #~ outpath_waterextent_months_jp2 = [a+b+c for a in [str(self.path.parent / (self.l2a_id_bef + "_waterextent_"))] \
                                                #~ for b in months \
                                                #~ for c in [".tif"]]
        outpath_waterextent_months_cnt_tif = [a+b+c for a in [str(self.path.parent / (self.l2a_id_bef + "_waterextent_"))] \
                                                for b in months \
                                                for c in ["_cnt.tif"]]
                                                
                                                
        waterextent_months_count = np.zeros((10980,10980,12), dtype=np.int16)
        nocloud_months_count = np.zeros((10980,10980,12), dtype=np.int16)
        
        logger.info("Reference water extent multiproc - Initializing shared variables")
        global shared_array_waterextent_months_count
        shared_array_waterextent_months_count_base = multiprocessing.Array(ctypes.c_int16, 10980 * 10980 * 12)
        shared_array_waterextent_months_count = np.ctypeslib.as_array(shared_array_waterextent_months_count_base.get_obj())
        shared_array_waterextent_months_count = shared_array_waterextent_months_count.reshape(10980, 10980, 12)
                
        global shared_array_nocloud_months_count
        shared_array_nocloud_months_count_base = multiprocessing.Array(ctypes.c_float, 10980 * 10980 * 12)
        shared_array_nocloud_months_count = np.ctypeslib.as_array(shared_array_nocloud_months_count_base.get_obj())
        shared_array_nocloud_months_count = shared_array_nocloud_months_count.reshape(10980, 10980, 12)
        
        logger.info("copyto")
        np.copyto(shared_array_waterextent_months_count, 0, where=True)
        np.copyto(shared_array_nocloud_months_count, 0, where=True)
        
        logger.info('Reference water extent multiproc - Starting multiprocessing...')
        
        pool = multiprocessing.Pool(processes = 4)

        #~ pool.map(partial(partial(we_multi, self), months_count), cva_list[::20])
        pool.map(partial(partial(we_multi, self), months_count), cva_list)
        
        pool.close()
        pool.join()

        logger.info("time: {}".format(timedelta(seconds = time.time() - start)))
        
        with rasterio.open(str(self.classif_SM_CM)) as self_classif_src:
            self_classif_profile = self_classif_src.profile
        self_classif_profile.update(driver="Gtiff",
                                    compress="DEFLATE",
                                    dtype=np.int16,
                                    transform=self_classif_src.transform,
                                    count=1)
        
        # Water Extent Total
        with np.errstate(divide='ignore', invalid='ignore'):
                waterextent_total_pct = (np.where(np.sum(shared_array_nocloud_months_count, 2) > 0, 
                                                         100*(np.sum(shared_array_waterextent_months_count, 2) / np.sum(shared_array_nocloud_months_count, 2)),
                                                         -1)).astype(np.int16)
                waterextent_total_cnt = np.sum(shared_array_nocloud_months_count, 2).astype(np.int16)
                                                             
        with rasterio.Env(GDAL_CACHEMAX=512) as env:
            with rasterio.open(str(outpath_waterextent_pct_tif), "w", **self_classif_profile) as dst:
                dst.write(waterextent_total_pct, 1)
            with rasterio.open(str(outpath_waterextent_cnt_tif), "w", **self_classif_profile) as dst:
                dst.write(waterextent_total_cnt, 1)
            
        # Water Extent Monthly    
        for i in range(len(months)):
            logger.info("Reference water extent - Processing {} water extent composite image from {} images".format(months[i], months_count[i]))

            with np.errstate(divide='ignore', invalid='ignore'):
                waterextent_month_pct = (np.where(shared_array_nocloud_months_count[:,:,i] > 0, 100*(shared_array_waterextent_months_count[:,:,i] / shared_array_nocloud_months_count[:,:,i]), -1)).astype(np.int16)
                waterextent_month_cnt = shared_array_nocloud_months_count[:,:,i].astype(np.int16)
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_waterextent_months_pct_tif[i]), "w", **self_classif_profile) as dst:
                    dst.write(waterextent_month_pct, 1) 
                with rasterio.open(str(outpath_waterextent_months_cnt_tif[i]), "w", **self_classif_profile) as dst:
                    dst.write(waterextent_month_cnt, 1) 
                
    
    def computeRefClassVariability(self):
        """
        """
        if self.date_cyclone:
            logger.info("Reference temporal context - using cyclone date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
        else:
            logger.info("Reference temporal context - cyclone date not set, using ref date for before/after comparison: {}".format(self.date_befaf.strftime("%Y%m%d")))
        
        outpath_folder = self._path.parent / "TEMPORAL_ANALYSIS" / "CLASS_VARIABILITY"
        outpath_folder.mkdir(parents = True, exist_ok = True)
        
        #~ outpath_nochange_tif = outpath_folder / (self.l2a_id_bef + "_classvar.tif")
        #~ outpath_nochange_jp2 = outpath_folder / (self.l2a_id_bef + "_classvar.jp2")
        
        lib = CVALibrary()
        cva_list = getattr(lib, self.tile)
        cva_list_sel = []
        for identifier in cva_list:
            try:
                product = CVAProduct(identifier)
                if product.l2a_id_bef == self.l2a_id_bef:
                    if product.reclassif:
                        cva_list_sel.append(product.cva_id)
            except:
                pass
        cva_list_sel = sorted(cva_list_sel, key=lambda x:x[72:])
        #~ logger.info(cva_list_sel)
        
        for index, identifier in enumerate(cva_list_sel[1:]):
            #~ logger.info("{} - {}".format(index, identifier))
            product = CVAProduct(identifier)
            logger.info("Reference Class Variability - {}/{} - {}".format(index+1, len(cva_list_sel)-1, product.cva_id))
            #~ logger.info("{}".format(product.reclassif))
            #~ logger.info("{}".format(CVAProduct(cva_list_sel[index]).reclassif))

            with rasterio.open(str(product.reclassif)) as prod_classif_src, \
                 rasterio.open(str(CVAProduct(cva_list_sel[index]).reclassif)) as previous_classif_src:
                prod_classif_profile = prod_classif_src.profile
                prod_classif = prod_classif_src.read(1).astype(np.int16)
                previous_classif = previous_classif_src.read(1).astype(np.int16)
            
            if index:
                previous_classif_cloudfilled = np.where(previous_classif !=-1, previous_classif, previous_classif_cloudfilled)
            else:
                previous_classif_cloudfilled = previous_classif
            
            new_class = np.where(np.logical_and(prod_classif != -1, previous_classif_cloudfilled != -1), np.where(prod_classif != previous_classif_cloudfilled, 1, 0), -1).astype(np.int16)
            
            try:
                new_class_total += np.where(new_class != -1, new_class, 0).astype(np.int16)
                nb_clouds_total += np.where(new_class != -1, 1, 0).astype(np.int16)
            except:
                new_class_total = np.where(new_class != -1, new_class, 0).astype(np.int16)
                nb_clouds_total = np.where(new_class != -1, 1, 0).astype(np.int16)
            
            outpath_classvar_tif = outpath_folder / (product.cva_id + "_classvar.tif")
            outpath_classvar_jp2 = outpath_folder / (product.cva_id + "_classvar.jp2")
            
            prod_classif_profile.update(driver="Gtiff",
                                        compress="NONE",
                                        dtype=np.int16,
                                        transform=prod_classif_src.transform,
                                        count=1)
            
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_classvar_tif), "w", **prod_classif_profile) as dst:
                    dst.write(new_class, 1)
            src_ds = gdal.Open(str(outpath_classvar_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_classvar_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_classvar_tif))
        
        outpath_classvar_total_tif = outpath_folder / (self.l2a_id_bef + "_classvar.tif")
        outpath_classvar_total_jp2 = outpath_folder / (self.l2a_id_bef + "_classvar.jp2")
        
        with np.errstate(divide='ignore', invalid='ignore'):
            classvar_pct = (np.where(nb_clouds_total > 0, 100*(new_class_total / nb_clouds_total), -1)).astype(np.int16)
        
        with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_classvar_total_tif), "w", **prod_classif_profile) as dst:
                    dst.write(classvar_pct, 1)
        src_ds = gdal.Open(str(outpath_classvar_total_tif))
        driver = gdal.GetDriverByName("JP2OpenJPEG")
        dst_ds = driver.CreateCopy(str(outpath_classvar_total_jp2), src_ds,
                                   options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
        dst_ds = None
        src_ds = None
        os.remove(str(outpath_classvar_total_tif))
            
            #~ Faire la somme totale
            #~ Faire la somme avant / après
        
        #~ count = 0
        #~ countbef = 0
        #~ countaft = 0

        
