#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for managing CVA products
"""

from pathlib import Path
from typing import List
import logging
import numpy as np
import rasterio
from osgeo import gdal
import os

from sen2chain import Tile, L2aProduct

from .cvaconfig import Config, SHARED_DATA
#~ from .cvaproduct import CVAProduct
#~ from .cvaprocess import computeCVA
#~ from .cvaclassif_multiproc import computeCVAClassif_multiproc
#~ from .cvaclassif import computeReclassif

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class CVALibrary:
    """Class for listing CVA, CVA_classif and more tiles in the library folders.

    """
    
    #~ _temp_path = Path(Config().get("temp_path"))
    #~ _l1c_path = Path(Config().get("l1c_path"))
    #~ _l2a_path = Path(Config().get("l2a_path"))
    #~ _indices_path = Path(Config().get("indices_path"))
    _cva_path = Path(Config().get("cva_path"))
        
    def __init__(self):
        self._cva = [f.name for f in self._cva_path.glob("*")]
        #~ logger.info(self._cva)
        for tile in self._cva:
            self.__dict__[tile] = []
            self.__dict__["_" + tile] = []
            for f in [a for a in ((self._cva_path / tile).glob("*")) if a.is_dir()]:
                self.__dict__[tile] += [f.name + "-" + g.name for g in [a for a in (self._cva_path / tile / f.name).glob("*") if a.is_dir()]]
                self.__dict__["_" + tile] += [f.name + "-" + g.name for g in [a for a in (self._cva_path / tile / f.name).glob("*") if a.is_dir()]]
            #~ self.__dict__[tile] = [f.name for f in (self._cva_path / tile).glob("*")]
            #~ self.__dict__["_"+tile] = [f.name for f in (self._cva_path / tile).glob("*")]
    
    @property
    def cva(self) -> List[str]:
        """Returns tiles in the CVA library folder."""
        return self._cva
        
    def computeCloudCover(self,
                          tile: str = None):
        
        outpath_tif = self._cva_path / tile / (tile + "_cloudcover.tif")
        outpath_jp2 = self._cva_path / tile / (tile + "_cloudcover.jp2")
        
        t = Tile(tile)
        logger.info("Found {} l2a products...".format(len(t.l2a)))
        count=0
        for l2a in [p.identifier for p in t.l2a]:
            cldmsk_path = t._paths['l2a'] / (l2a[:-5] + "_CLOUD_MASK_B11.jp2")
            if cldmsk_path.exists():
                count += 1
                logger.info("{}: {}".format(count, l2a))
                l2a_product = L2aProduct(l2a)
                with rasterio.open(str(cldmsk_path)) as cldmsk_src, \
                     rasterio.open(l2a_product.aot_20m) as aot_src:
                    
                    cldmsk_profile = cldmsk_src.profile
                    cldmsk = cldmsk_src.read(1).astype(np.int16)
                    aot = aot_src.read(1).astype(np.int16)
                try:
                    cloud_count += 1*(cldmsk == 1)
                    data_count += 1*(aot != 0)
                except:
                    cloud_count = 1*(cldmsk == 1)
                    data_count = 1*(aot != 0)
        if count:
            logger.info("Compiled {} cloud masks B11".format(count))
            cloud_pct = (100 * cloud_count / data_count).astype(np.int8)
            cldmsk_profile.update(driver="Gtiff",
                                  compress="NONE",
                                  tiled=False,
                                  dtype=np.int8,
                                  nodata=0,
                                  transform=cldmsk_src.transform,
                                  count=1)
            cldmsk_profile.pop('tiled', None)
            cldmsk_profile.pop('nodata', None)
            
            outpath_tif.parent.mkdir(parents = True, exist_ok = True)
            
            with rasterio.Env(GDAL_CACHEMAX=512) as env:
                with rasterio.open(str(outpath_tif), "w", **cldmsk_profile) as dst:
                    dst.write(cloud_pct, 1)
            
            #~ logger.info("Converting to JPEG2000")
            src_ds = gdal.Open(str(outpath_tif))
            driver = gdal.GetDriverByName("JP2OpenJPEG")
            dst_ds = driver.CreateCopy(str(outpath_jp2), src_ds,
                                       options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
            dst_ds = None
            src_ds = None
            os.remove(str(outpath_tif))
        else:
            logger.info("No cloud masks B11")
    
    def computeCVA(self,
                   tile: str = None,
                   ref: str = None):
        if not tile or not ref:
            logger.info("Please provide tile and reference identifier\nTile: {}\nRef: {}".format(tile, ref))
        else:
            from .cvaproduct import CVAProduct
            tile = Tile(tile)
            nb = len(tile.l2a)-1
            logger.info("Computing {} CVA products for tile {}".format(nb, tile))
            logger.info("Ref: {}".format(ref))
            n=0
            for l2a in tile.l2a:
                if ref != l2a.identifier[:-5]:
                    n+=1
                    cva_id = ref + "-" + l2a.identifier[:-5]
                    logger.info("{}/{} {}".format(n,nb, cva_id))
                    product = CVAProduct(cva_id)
                    if not product.cva:
                        product.initialize()
                        product.computeCVA()
                    if not product.classif:
                        product.computeClassif(removecva=True)
                    if not product.classif_SM:
                        product.computeSeuillageMagnitude()
                    if not product.classif_SM_CM:
                        product.computeCloudMaskClassif()
                    if not product.reclassif:
                        product.computeReclassif()
  
