#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Module for computing Change Vector Analysis using multiprocessing
"""

import logging
from pathlib import Path
import pandas as pd
import rasterio
import multiprocessing
import ctypes
from functools import partial
import time
from datetime import timedelta
import numpy as np
from osgeo import gdal
import os

from .cvaclassif import legend_classif
from .cvaconfig import Config, SHARED_DATA

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


global shared_array_classification
shared_array_classification = None
#~ shared_array_classification_base = multiprocessing.Array(ctypes.c_int16, 10980 * 10980)
#~ shared_array_classification = np.ctypeslib.as_array(shared_array_classification_base.get_obj())
#~ shared_array_classification = shared_array_classification.reshape(10980, 10980)
#~ logger.info('0 shared_array_classification id: {}'.format(id(shared_array_classification)))
#~ logger.info('0 shared_array_classification: {}'.format((shared_array_classification)))




global shared_array_distance
shared_array_distance = None
#~ shared_array_distance_base = multiprocessing.Array(ctypes.c_float, 10980 * 10980)
#~ shared_array_distance = np.ctypeslib.as_array(shared_array_distance_base.get_obj())
#~ shared_array_distance = shared_array_distance.reshape(10980, 10980)
#~ np.place(shared_array_distance, 0 == shared_array_distance, 99999)
#~ np.copyto(shared_array_distance, 99999, where=True)
#~ logger.info('0 shared_array_distance id: {}'.format(id(shared_array_distance)))
#~ logger.info('0 shared_array_distance: {}'.format(shared_array_distance))


global data_dict_global
data_dict_global = None

global lock
lock = multiprocessing.Lock()

def class_and_d_multi(list_element, def_param=(lock, shared_array_distance, shared_array_classification, data_dict_global)):   
    global lock
        
    i = list_element[0]
    cclass = list_element[1]

    logger.info('{} - {}'.format(i+1, cclass))

    global data_dict_global
    
    global shared_array_distance
    global shared_array_classification

    
    d = ((data_dict_global['cosx'] - 10000 * data_dict_global['df'].loc[cclass]['cos_NDVI_diff'])**2 + 
        (data_dict_global['cosy'] - 10000 * data_dict_global['df'].loc[cclass]['cos_BIGR_diff'])**2 + 
        (data_dict_global['cosz'] - 10000 * data_dict_global['df'].loc[cclass]['cos_NDWIGAO_diff'])**2 + 
        (data_dict_global['cosx_mean'] - 10000 * data_dict_global['df'].loc[cclass]['cos_NDVI_mean'])**2 +
        (data_dict_global['cosy_mean'] - 10000 * data_dict_global['df'].loc[cclass]['cos_BIGR_mean'])**2 + 
        (data_dict_global['cosz_mean'] - 10000 * data_dict_global['df'].loc[cclass]['cos_NDWIGAO_mean'])**2
        + (data_dict_global['magnitude_mean'] - data_dict_global['df'].loc[cclass]['magnitude_mean'])**2 \
        )**0.5
    
    lock.acquire() 
    np.copyto(shared_array_classification, i+1, where=(d <= shared_array_distance))
    np.copyto(shared_array_distance, d, where=(d <= shared_array_distance))
    lock.release()
    
#~ def rasterio_read(shared_dict, proc_item):
    #~ global cva_src
    #~ logger.info('Loading {}...'.format(proc_item[0]))
    #~ shared_dict[proc_item[0]] = cva_src.read(proc_item[1]).astype(np.float32)
    
def save_to_jp2(infile, outfile):
    logger.info("converting {} file...".format(infile))

    src_ds = gdal.Open(infile)
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(outfile, src_ds,
                               options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"])
    dst_ds = None
    src_ds = None
    
def classif_CVA_multiproc(cva_image_path: str, 
                cva_changelib_path: str = None
                ):
    """
    Usage:
    """    
    
    start = time.time()
    
    out_path_class = Path(cva_image_path).parent / (Path(cva_image_path).stem + '_classification_multiproc.tif')
    out_path_class_jp2 = Path(cva_image_path).parent / (Path(cva_image_path).stem + '_classification_multiproc.jp2')
    out_path_dist = Path(cva_image_path).parent / (Path(cva_image_path).stem + '_distance_multiproc.tif')
    out_path_dist_jp2 = Path(cva_image_path).parent / (Path(cva_image_path).stem + '_distance_multiproc.jp2')  
        
    if cva_changelib_path is None:
        cva_changelib_path = Path(Config().get("cva_changelib_path"))
    df = pd.read_csv(cva_changelib_path)
    df = df.set_index('ChangeClass')
    df = df
    data_dict = {'df': df}
    with rasterio.open(cva_image_path) as cva_src:
        # Pour lire les bandes en parallèle avec Rasterio 
        # mais non efficace car Rasterio semble déjà lire les données en mutiprocessing
        #~ global cva_src
        #~ manager = multiprocessing.Manager()
        #~ shared_dict = manager.dict()
        #~ load_list = [('cosx', 2), ('cosy', 3), ('cosz', 4), ('magnitude_mean', 8), ('cosx_mean', 9), ('cosy_mean', 10), ('cosz_mean', 11)]
        #~ pool = multiprocessing.Pool(8)
        #~ results = [pool.map(partial(rasterio_read, shared_dict), load_list)]
        #~ data_dict.update(shared_dict)
        
        #~ logger.info('Loading magnitude...')
        #~ magnitude = cva_src.read(1).astype(np.float32)
        logger.info('Loading cos x...')
        data_dict['cosx'] = cva_src.read(2).astype(np.float32)
        logger.info('Loading cos y...')
        data_dict['cosy'] = cva_src.read(3).astype(np.float32)
        logger.info('Loading cos z...')
        data_dict['cosz'] = cva_src.read(4).astype(np.float32)
        logger.info('Loading mediane magnitude...')
        data_dict['magnitude_mean'] = cva_src.read(8).astype(np.float32)
        logger.info('Loading mediane cos x...')
        data_dict['cosx_mean'] = cva_src.read(9).astype(np.float32)
        logger.info('Loading mediane cos y...')
        data_dict['cosy_mean'] = cva_src.read(10).astype(np.float32)
        logger.info('Loading mediane cos z...')
        data_dict['cosz_mean'] = cva_src.read(11).astype(np.float32)
        
        logger.info("time: {}".format(timedelta(seconds = time.time() - start)))
        
        magn_profile = cva_src.profile
        width = magn_profile['width']
        height = magn_profile['height']
        
        logger.info('Computing shared objects...')

        global shared_array_classification
        shared_array_classification_base = multiprocessing.Array(ctypes.c_int16, 10980 * 10980)
        shared_array_classification = np.ctypeslib.as_array(shared_array_classification_base.get_obj())
        shared_array_classification = shared_array_classification.reshape(10980, 10980)
                
        global shared_array_distance
        shared_array_distance_base = multiprocessing.Array(ctypes.c_float, 10980 * 10980)
        shared_array_distance = np.ctypeslib.as_array(shared_array_distance_base.get_obj())
        shared_array_distance = shared_array_distance.reshape(10980, 10980)
                
        np.copyto(shared_array_classification, 0, where=True)
        np.copyto(shared_array_distance, 99999, where=True)

        global data_dict_global
        data_dict_global = data_dict
        
        logger.info('Starting multiprocessing...')
        
        pool = multiprocessing.Pool(processes = 8)
        
        index_list = list(enumerate(df.index))

        pool.map(class_and_d_multi, index_list)
        
        pool.close()
        pool.join()
    
    logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

    logger.info("saving TIFF files...")
    magn_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.int16,
                       transform=cva_src.transform,
                       count=1)
    magn_profile.pop('tiled', None)
    magn_profile.pop('nodata', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_class), "w", **magn_profile) as dst:
            dst.write(shared_array_classification.astype(np.int16), 1)
            
    magn_profile.update(dtype=np.int16)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_dist), "w", **magn_profile) as dst:
            dst.write(shared_array_distance.astype(np.int16), 1)

    logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

    logger.info("converting TIFF files to JPEG2000")
    
    p1 = multiprocessing.Process(target = save_to_jp2, args = (str(out_path_class), str(out_path_class_jp2), ))
    p1.start()
    p2 = multiprocessing.Process(target = save_to_jp2, args = (str(out_path_dist), str(out_path_dist_jp2), ))
    p2.start()
    p1.join() 
    p2.join()
    
    os.remove(str(out_path_dist))
    os.remove(str(out_path_class))
    
    legend_classif(class_image_path = str(out_path_class_jp2), \
                   cva_changelib_path = cva_changelib_path)
                   
    end = time.time()
    logger.info("time: {}".format(timedelta(seconds = end - start)))


def computeCVAClassif_multiproc(cva_prod = None): 
#~ def classif_CVA_multiproc(cva_image_path: str, 
                #~ cva_changelib_path: str = None
                #~ ):
    """
    Usage:
    """    
    
    start = time.time()
    
    out_folder = cva_prod.path
    
    out_path_class = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_classif.tif")
    out_path_class_jp2 = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_classif.jp2")
    out_path_dist = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_distance.tif")
    out_path_dist_jp2 = Path(out_folder) / ("CVA3D_" + cva_prod.cva_id + "_distance.jp2")
        
    #~ if cva_changelib_path is None:
    cva_changelib_path = Path(Config().get("cva_changelib_path"))
    df = pd.read_csv(cva_changelib_path)
    df = df.set_index('ChangeClass')
    df = df
    data_dict = {'df': df}
    with rasterio.open(str(cva_prod.cva)) as cva_src:
        #~ logger.info('Loading magnitude...')
        #~ magnitude = cva_src.read(1).astype(np.float32)
        logger.info('Loading cos x...')
        data_dict['cosx'] = cva_src.read(1).astype(np.float32)
        logger.info('Loading cos y...')
        data_dict['cosy'] = cva_src.read(2).astype(np.float32)
        logger.info('Loading cos z...')
        data_dict['cosz'] = cva_src.read(3).astype(np.float32)
        logger.info('Loading mediane magnitude...')
        data_dict['magnitude_mean'] = cva_src.read(7).astype(np.float32)
        logger.info('Loading mediane cos x...')
        data_dict['cosx_mean'] = cva_src.read(8).astype(np.float32)
        logger.info('Loading mediane cos y...')
        data_dict['cosy_mean'] = cva_src.read(9).astype(np.float32)
        logger.info('Loading mediane cos z...')
        data_dict['cosz_mean'] = cva_src.read(10).astype(np.float32)
        
        logger.info("time: {}".format(timedelta(seconds = time.time() - start)))
        
        magn_profile = cva_src.profile
        width = magn_profile['width']
        height = magn_profile['height']
        
        logger.info('Computing shared objects...')

        global shared_array_classification
        shared_array_classification_base = multiprocessing.Array(ctypes.c_int16, 10980 * 10980)
        shared_array_classification = np.ctypeslib.as_array(shared_array_classification_base.get_obj())
        shared_array_classification = shared_array_classification.reshape(10980, 10980)
                
        global shared_array_distance
        shared_array_distance_base = multiprocessing.Array(ctypes.c_float, 10980 * 10980)
        shared_array_distance = np.ctypeslib.as_array(shared_array_distance_base.get_obj())
        shared_array_distance = shared_array_distance.reshape(10980, 10980)
                
        np.copyto(shared_array_classification, 0, where=True)
        np.copyto(shared_array_distance, 99999, where=True)

        global data_dict_global
        data_dict_global = data_dict
        
        logger.info('Starting multiprocessing...')
        
        pool = multiprocessing.Pool(processes = 8)
        
        index_list = list(enumerate(df.index))

        pool.map(class_and_d_multi, index_list)
        
        pool.close()
        pool.join()
    
    logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

    logger.info("saving TIFF files...")
    magn_profile.update(driver="Gtiff",
                       compress="NONE",
                       tiled=False,
                       dtype=np.int16,
                       transform=cva_src.transform,
                       count=1)
    magn_profile.pop('tiled', None)
    magn_profile.pop('nodata', None)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_class), "w", **magn_profile) as dst:
            dst.write(shared_array_classification.astype(np.int16), 1)
            
    magn_profile.update(dtype=np.int16)
    
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path_dist), "w", **magn_profile) as dst:
            dst.write(shared_array_distance.astype(np.int16), 1)

    logger.info("time: {}".format(timedelta(seconds = time.time() - start)))

    logger.info("converting TIFF files to JPEG2000")
    
    p1 = multiprocessing.Process(target = save_to_jp2, args = (str(out_path_class), str(out_path_class_jp2), ))
    p1.start()
    p2 = multiprocessing.Process(target = save_to_jp2, args = (str(out_path_dist), str(out_path_dist_jp2), ))
    p2.start()
    p1.join() 
    p2.join()
    
    os.remove(str(out_path_dist))
    os.remove(str(out_path_class))
    
    legend_classif(class_image_path = str(out_path_class_jp2), \
                   cva_changelib_path = cva_changelib_path)
                   
    end = time.time()
    logger.info("time: {}".format(timedelta(seconds = end - start)))
